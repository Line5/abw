﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotateCam : MonoBehaviour
{
	public FireAndFan[] fanContainer;
	
	public GameObject point;
	public GameObject player;
	private float isoDeg;
	public float speed = 1;
	
	private Vector3 camStart;
	private Quaternion camQua;
	private bool camLock = true;
	private bool lastPosLock;
	private bool posLockChanged = false;

	private bool rotateOn = false;

	private GameObject blockOccupied;
	private GameObject blockNextLeft;

	private PlayerMovement playerMovement;
	private PlayerSwipe playerSwipe;
	private NeighbourLeftSystem neighbourLeftSystem;

	public Quaternion CamQua
	{
		get
		{
			return camQua;
		}
	}

	public bool CamLock
	{
		get
		{
			return camLock;
		}
		set
		{
			camLock = value;
		}
	}

	public bool PosLockChanged
	{
		get
		{
			return posLockChanged;
		}
	}

	void Awake()
	{
		playerSwipe = player.GetComponent<PlayerSwipe> ();
		playerMovement = player.GetComponent<PlayerMovement> ();
		neighbourLeftSystem = player.GetComponent<NeighbourLeftSystem> ();

		isoDeg = Mathf.Rad2Deg*(Mathf.Asin(Mathf.Sqrt(3)/3));
		camStart = new Vector3 (isoDeg, 45f, 0f);
		transform.eulerAngles = camStart;
		camQua = transform.rotation;
//		RecalculateNeighbours ();
	}
	void Start()
	{
//		RecalculateNeighbours ();
	}

	public bool CheckForRotate()
	{
		blockOccupied = GameObject.FindGameObjectWithTag ("Occupied");
		blockNextLeft = GameObject.FindGameObjectWithTag ("NextLeft");
		if (blockOccupied != null && blockNextLeft != null)
		{
			if (blockNextLeft.transform.position.y == blockNextLeft.transform.position.y)
				return true;
			else
				return false;
		}
		else if (blockOccupied != null && blockNextLeft == null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public IEnumerator RotateCamera(float rotA)
	{
		AudioManager.instance.PlaySFXClip(AudioManager.instance.fanSwitch,AudioManager.instance.sfxMixer);

		if (!rotateOn)
		{
			rotateOn = true;
			Vector3 pin = point.transform.position;
			Vector3 axis = Vector3.up;
			float rotT = speed;

			float step = 0f;
			float rate = 1f / rotT;
			float smoothStep = 0f;
			float lastStep = 0f;
			while (step < 1f) 
			{
				CamLock = false;
				step += Time.deltaTime * rate;
				smoothStep = Mathf.SmoothStep (0f, 1f, step);
				transform.RotateAround(pin, axis, rotA * (smoothStep - lastStep));
				lastStep = smoothStep;
				yield return new WaitForEndOfFrame();
			}
			if (step > 1f) transform.RotateAround (pin, axis, rotA * (1f - lastStep));
				camQua = gameObject.transform.rotation;
			CamLock = true;

			playerMovement.PlayerStopped = false;
			playerSwipe.SwipeOn = false;

			playerMovement.NextLeftPointSystemReference = null;
			playerMovement.StopAllCoroutines ();

			if (fanContainer.Length > 0)
			{
				foreach (FireAndFan script in fanContainer)
					script.StartCoroutine ("DoFan");
			}

			neighbourLeftSystem.ReCall ();

			rotateOn = false;
		}
	}

	public void RecalculateNeighbours ()
	{
		StartCoroutine (Recalculate());
	}

	IEnumerator Recalculate ()
	{
		posLockChanged = true;
		yield return new WaitForEndOfFrame();
		posLockChanged = false;
	}
}