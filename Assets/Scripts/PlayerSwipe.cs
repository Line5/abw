﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerSwipe : MonoBehaviour
{
	RotateCam rotateCam;
	PlayerMovement playerMovement;

	public int swipeRadius = 10;
	private float swipeRadiusPercentage;

	private bool swipeOn = false;

	private Vector2 mouseDownPos;
	private Vector2 mouseNowPos;
	private Vector2 mouseLastPos;

	private bool swipedLeft = false;
	private bool swipedRight = false;

	private GameObject speedSlider;
	private GameObject pauseFrame;

	public bool SwipeOn
	{
		get
		{
			return swipeOn;
		}
		set
		{
			swipeOn = value;
		}
	}

	void Awake ()
	{
		rotateCam = GameObject.Find ("Camera").GetComponent<RotateCam> ();
		playerMovement = this.GetComponent<PlayerMovement> ();
		
		speedSlider = GameObject.Find ("Slider");
		pauseFrame = GameObject.Find ("Frame");
	
		swipeRadius = Mathf.Clamp (swipeRadius, 1, 100);
		swipeRadiusPercentage = (Screen.width / 100) * swipeRadius;
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown(0) && rotateCam.CheckForRotate())
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			Physics.Raycast (ray.origin, ray.direction, out hit, Mathf.Infinity);


			if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != speedSlider && UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != pauseFrame)
			{

	//			if (!swipeOn && (hit.collider == null || ((hit.collider.name != "UiColliders" && hit.collider.gameObject.layer != 4) && (hit.collider.name != "Rotate" && hit.collider.name != "Slide"))))
				if (hit.collider != null && (hit.collider.gameObject.name != "Rotate" && hit.collider.gameObject.name != "Slide"))
				{
					swipeOn = true;
					mouseDownPos = Input.mousePosition;
				}
				else if (hit.collider == null)
				{
					swipeOn = true;
					mouseDownPos = Input.mousePosition;
				}
			}
			else
				print ("hit ui");
		}

		if (Input.GetMouseButtonUp (0) && swipeOn)
		{
			bool horizontal = false;

			mouseNowPos = Input.mousePosition;

			if (Mathf.Abs(mouseNowPos.x - mouseDownPos.x) > Mathf.Abs(mouseNowPos.y - mouseDownPos.y))
			{
				horizontal = true;
			}

			if (horizontal)
			{
				if (mouseDownPos.y <= Screen.height / 2)
				{
					if (mouseNowPos.x < mouseDownPos.x - swipeRadiusPercentage)
					{
						swipedLeft = true;
						playerMovement.PlayerStopped = true;
					}
					else if (mouseNowPos.x > mouseDownPos.x + swipeRadiusPercentage)
					{
						swipedRight = true;
						playerMovement.PlayerStopped = true;
					}
				}
				else
				{
					if (mouseNowPos.x < mouseDownPos.x - swipeRadiusPercentage)
					{
						swipedRight = true;
						playerMovement.PlayerStopped = true;
					}
					else if (mouseNowPos.x > mouseDownPos.x + swipeRadiusPercentage)
					{
						swipedLeft = true;
						playerMovement.PlayerStopped = true;
					}
				}
			} else
			{

				if (mouseDownPos.x <= Screen.width / 2)
				{
					if (mouseNowPos.y < mouseDownPos.y - swipeRadiusPercentage)
					{
						swipedRight = true;
						playerMovement.PlayerStopped = true;
					}
					else if (mouseNowPos.y > mouseDownPos.y + swipeRadiusPercentage)
					{
						swipedLeft = true;
						playerMovement.PlayerStopped = true;
					}
				}
				else
				{
					if (mouseNowPos.y < mouseDownPos.y - swipeRadiusPercentage)
					{
						swipedLeft = true;
						playerMovement.PlayerStopped = true;
					}
					else if (mouseNowPos.y > mouseDownPos.y + swipeRadiusPercentage)
					{
						swipedRight = true;
						playerMovement.PlayerStopped = true;
					}
				}
			}
		}

		if (swipedLeft && playerMovement.NotMoving)
		{
			swipedLeft = false;
			StartCoroutine(rotateCam.RotateCamera(-90));
		}

		if (swipedRight && playerMovement.NotMoving)
		{
			swipedRight = false;
			StartCoroutine(rotateCam.RotateCamera(90));
		}
	}

}
