﻿using UnityEngine;
using System.Collections;

public class AnimationSpeed : MonoBehaviour {

	public float regularSpeed;
	public float glitchSpeed;
	public Animator[] myAnim;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.A)) {
			Glitch();
		}
	
	}

	public void Glitch()
	{
		foreach (Animator anim in myAnim) {

			StartCoroutine(GlitchSequence(anim));

		}
	}

	public IEnumerator GlitchSequence(Animator anim)
	{
		print ("glitching");
		anim.speed = Random.Range(-10.5f,10f);

		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));

		anim.speed = Random.Range(-10.5f,10f);
		
		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));
		anim.speed = Random.Range(-10.5f,10f);
		
		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));
		anim.speed = Random.Range(-10.5f,10f);
		
		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));
		anim.speed = Random.Range(-10.5f,10f);
		
		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));
		anim.speed = Random.Range(-10.5f,10f);
		
		yield return new WaitForSeconds (Random.Range(0.1f,0.5f));
		print ("done glitching");
		anim.speed = 1;
	}
	
}
