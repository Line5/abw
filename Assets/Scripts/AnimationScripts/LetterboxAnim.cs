﻿using UnityEngine;
using System.Collections;

public class LetterboxAnim : MonoBehaviour {

	private Animator openMail;
	private float timer;

	public Transform mail;
	public Transform letterboxPoint;
	public Transform letterprinterPoint;
	public Transform letterboxMidpoint; 
	public float timeBetweenMail = 1.5f;
	public float timeToPlayMailPointA = 2.5f;
	public float timeToPlayMailPointB = 2.5f;


	// Use this for initialization
	void Start () {
		openMail = GetComponent<Animator> ();
		StartCoroutine (TimeStuff ());


	}

	IEnumerator MailTravel () {
		print ("Did stuff yo!");
		Vector3 pos = letterprinterPoint.position;
		for(float I = 0f; I < timeToPlayMailPointA; I += Time.deltaTime) 
		{
			pos = Vector3.Lerp(letterprinterPoint.position, letterboxMidpoint.position, I/ timeToPlayMailPointA);
			mail.position = pos;
			mail.LookAt(letterboxMidpoint);

			yield return null;

		}

		for(float I = 0f; I < timeToPlayMailPointB; I += Time.deltaTime) 
		{
			pos = Vector3.Lerp(letterboxMidpoint.position, letterboxPoint.position, I/ timeToPlayMailPointB);
			mail.position = pos;
			mail.LookAt(letterboxPoint);

			yield return null;
			
		}


		openMail.Play ("MailboxOpen");

		StartCoroutine (TimeStuff ());
		yield break;	
	}

	IEnumerator TimeStuff () {
		for(float I = 0f; I <  timeBetweenMail; I += Time.deltaTime) 
		{
				
			yield return null;
			
		}

		StartCoroutine(MailTravel());
		yield break;
	}
	// Update is called once per frame
	void Update () {

	}
}
