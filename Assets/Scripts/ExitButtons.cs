﻿using UnityEngine;
using System.Collections;

public class ExitButtons : MonoBehaviour
{
	public int numberOfButtons = 0;
	private int numberOfButtonsOn = 0;
	public GameObject exitBlock;
	private GameObject exitPointSystem;
	private Animator exitAnimator;

	private bool once = true;

	public void Start()
	{
		if (exitBlock != null)
		{
			foreach(Transform transform in exitBlock.transform)
			{
				if (transform.name == "PointSystem")
					exitPointSystem = transform.gameObject;
			}
			exitAnimator = this.gameObject.GetComponent<Animator> ();
		}
	}

	public void SwitchOn()
	{
		if (exitBlock != null)
			numberOfButtonsOn ++;
	}
	
	public void SwitchOff()
	{
		if (exitBlock != null)
		{
			numberOfButtonsOn --;
			if (numberOfButtonsOn < 0)
				numberOfButtonsOn = 0;
		}
	}

	public void Update()
	{
		if (exitBlock != null)
		{
			if (numberOfButtons == 0)
			{
				if(numberOfButtonsOn == 0)
					exitAnimator.SetTrigger ("state0");
			}
			
			if (numberOfButtons == 1)
			{
				if(numberOfButtonsOn == 0)
				{
					exitAnimator.SetTrigger ("state100");
					once = true;
				}
				if(numberOfButtonsOn == 1)
				{
					exitAnimator.SetTrigger ("state0");
					if (once)
					{
						AudioManager.instance.PlaySFXClip(AudioManager.instance.beep1,AudioManager.instance.sfxMixer);
						once = false;
					}
				}
			}
			
			if (numberOfButtons == 2)
			{
				if(numberOfButtonsOn == 0)
					exitAnimator.SetTrigger ("state100");
				if(numberOfButtonsOn == 1)
				{
					exitAnimator.SetTrigger ("state50");
					once = true;
				}
				if(numberOfButtonsOn == 2)
				{
					exitAnimator.SetTrigger ("state0");
					if (once)
					{
						AudioManager.instance.PlaySFXClip(AudioManager.instance.beep1,AudioManager.instance.sfxMixer);
						once = false;
					}
				}
			}
			
			if (numberOfButtons == 3)
			{
				if(numberOfButtonsOn == 0)
					exitAnimator.SetTrigger ("state100");
				if(numberOfButtonsOn == 1)
					exitAnimator.SetTrigger ("state66");
				if(numberOfButtonsOn == 2)
				{
					exitAnimator.SetTrigger ("state33");
					once = true;
				}
				if(numberOfButtonsOn == 3)
				{
					exitAnimator.SetTrigger ("state0");
					if (once)
					{
						AudioManager.instance.PlaySFXClip(AudioManager.instance.beep1,AudioManager.instance.sfxMixer);
						once = false;
					}
				}
			}

			if (numberOfButtons == 4)
			{
				if(numberOfButtonsOn == 0)
					exitAnimator.SetTrigger ("state100");
				if(numberOfButtonsOn == 1)
					exitAnimator.SetTrigger ("state75");
				if(numberOfButtonsOn == 2)
					exitAnimator.SetTrigger ("state50");
				if(numberOfButtonsOn == 3)
				{
					exitAnimator.SetTrigger ("state25");
					once = true;
				}
				if(numberOfButtonsOn == 4)
				{
					exitAnimator.SetTrigger ("state0");
					if (once)
					{
						AudioManager.instance.PlaySFXClip(AudioManager.instance.beep1,AudioManager.instance.sfxMixer);
						once = false;
					}
				}
			}

			if (numberOfButtons == numberOfButtonsOn || numberOfButtons < numberOfButtonsOn)
			{
				StartCoroutine (Pause(true));
			}
			else
			{
				StartCoroutine (Pause(false));
			}
		}
	}

	IEnumerator Pause(bool boolean) {
		yield return new WaitForSeconds(0.5f);
		foreach (Transform point in exitPointSystem.transform)
			point.gameObject.SetActive (boolean);
	}
}
