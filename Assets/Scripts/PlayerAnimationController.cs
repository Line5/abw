﻿using UnityEngine;
using System.Collections;

public class PlayerAnimationController : MonoBehaviour 
{
	public static PlayerAnimationController instance;
	public Animator myAnimator;

	public void WalkAnimSwitcher ()
	{
		float value = GetComponentInParent<PlayerMovement> ().speedSlider.value;

		if (value >= 0 && value < 2f) 
		{
			WalkCycleNormal ();
		}

		else if (value >= 2f && value < 3.5f) 
		{
			WalkCycleMedium ();
		}

		else if (value >= 3.5f && value <= 5) 
		{
			WalkCycleFast ();
		}
	}
	 
	public void WalkCycleNormal ()
	{
		myAnimator.SetTrigger ("Normal Walk Cycle");
		myAnimator.SetInteger ("Walk Speed",0);
	}

	public void WalkCycleMedium ()
	{
		myAnimator.SetTrigger ("Medium Walk Cycle");
		myAnimator.SetInteger ("Walk Speed",1);
	}

	public void WalkCycleFast ()
	{
		myAnimator.SetTrigger ("Fast Walk Cycle");
		myAnimator.SetInteger ("Walk Speed",2);
	}

	public void Idle ()
	{
		myAnimator.SetTrigger ("Idle");
	}

	public void TeleportStretch ()
	{
		myAnimator.SetTrigger ("Teleport Stretch");
	}

	public void ExitTeleport()
	{
		myAnimator.SetBool ("EndGame", true);
	}
	void Awake ()
	{
		instance = this;
	}
	// Use this for initialization
	void Start () 
	{

		myAnimator = gameObject.GetComponent<Animator> ();
		myAnimator.enabled = true;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
