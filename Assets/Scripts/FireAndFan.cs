﻿using UnityEngine;
using System.Collections;

public class FireAndFan : MonoBehaviour
{
	public bool buttonOn = false;
	private bool alreadyRunning = false;

	private GameObject windParticles;

	public GameObject pointContainer;
	public GameObject fanModel;
	private float newAngle = 0.0f;
	private float lastAngle = 1.0f;
	
	private IEnumerator doFan;

	void Start()
	{
		foreach (Transform transform in this.transform)
			if (transform.name == "Wind_Particle")
				windParticles = transform.gameObject;
	}

	void Update()
	{
		RotateFanPoints ();
	}

	public void SwitchOn()
	{
		buttonOn = true;

		windParticles.GetComponent<Animator>().SetTrigger ("windOn");

		StartCoroutine ("DoFan");
	}

	public void SwitchOff()
	{
		buttonOn = false;

		windParticles.GetComponent<Animator>().SetTrigger ("windOff");

		doFan = DoFan ();
		StopAllCoroutines ();
	}

	private void RotateFanPoints()
	{
		float cameraAngle = Camera.main.transform.rotation.eulerAngles.y;

		if(cameraAngle != lastAngle)
		{
			if (0.0f < cameraAngle && cameraAngle <= 90.0f)
				newAngle = 0.0f;
			if (90.0f < cameraAngle && cameraAngle <= 180.0f)
				newAngle = 90.0f;
			if (180.0f < cameraAngle && cameraAngle <= 270.0f)
				newAngle = 180.0f;
			if (270.0f < cameraAngle || cameraAngle <= 0.0f)
				newAngle = 270.0f;

			lastAngle = cameraAngle;
		}

		Quaternion newRotation = Quaternion.Euler(0.0f, newAngle, 0.0f);

		pointContainer.transform.rotation = newRotation;
	}

	public IEnumerator DoFan()
	{
		if (buttonOn)
		{
			RotateFanPoints ();

			yield return new WaitForSeconds(0.5f);

			if(buttonOn)
			{
				foreach (Transform point in pointContainer.transform)
				{
					RaycastHit[] hits;
					hits = Physics.RaycastAll(point.position, fanModel.transform.TransformDirection(Vector3.forward), 20.0f);
					Debug.DrawRay(point.position, fanModel.transform.TransformDirection(Vector3.forward) * 20.0f, Color.green, 5.0f);

					for (int i = 0; i < hits.Length; i++)
					{
						RaycastHit hit = hits[i];
						if (hit.collider.transform.parent.parent != null)
						{
							if (hit.collider.transform.parent.parent.name == "Fire")
							{
								hit.collider.transform.parent.gameObject.GetComponent<SecondaryBlocks>().fireClear = true;
							}
						}
					}
				}
			}
		}
	}
}
