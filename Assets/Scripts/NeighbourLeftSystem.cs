﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NeighbourLeftSystem : MonoBehaviour
{
	// Materials for Debug
	public bool debugOn;
	public Material materialOccupied;
	public Material materialNeighbour;
	public Material materialNextLeft;
	public Material materialUntagged;
	public Material materialLastOccupied;

	private RotateCam rotateCam; // Script reference
	private PlayerMovement playerMovement;

	private GameObject occupiedPointSystem; // PointSystem attached to the path that the character is currently colliding with
	private GameObject lastOccupiedPointSystem;
	private GameObject holder; //test holder

	private GameObject occupiedCollider; // ColliderPoint in occupied PointSystem
	private List <GameObject> occupiedCastPointList = new List<GameObject>(); // List of CastPoints in occupied PointSystem
	private GameObject occupiedCastPointPosX; // Furthest Occupied CastPoint on the + X axis [0]
	private GameObject occupiedCastPointPosZ; // Furthest Occupied CastPoint on the + Z axis [3]
	private GameObject occupiedCastPointNegZ; // Furthest Occupied CastPoint on the - Z axis [1]
	private GameObject occupiedCastPointNegX; // Furthest Occupied CastPoint on the - X axis [2]

	private GameObject[] neighbourPointSystems = new GameObject[4] {null, null, null, null};

	private int idNext = -1;
	private int lastIdNext;
	private int idLast = -1;
	
	TransformableIsland tiScript;
	TransformableIsland temp;

	private GameObject level;

	public int IdNext
	{
		get
		{
			return idNext;
		}
	}

	void Start()
	{
		rotateCam = GameObject.Find ("Camera").GetComponent<RotateCam> ();
		playerMovement = this.GetComponent<PlayerMovement> ();
		level = GameObject.Find ("Level");
		ReCall ();
	}

	void Update()
	{
		foreach (Transform group in level.transform)
		{
			foreach (Transform path in group)
			{
				foreach (Transform child in path)
				{
					if (child.name == "PointSystem")
					{
						foreach (Transform point in child)
						{
							MeshRenderer renderer = point.GetComponent<MeshRenderer>();
							
							if(debugOn)
								renderer.enabled = true;
							else
								renderer.enabled = false;
						}
					}
				}
			}
		}
		if (debugOn)
			ColourNeighbours();
	}

	public void ReCall()
	{	
		FindOccupied();

//		if (lastOccupiedPointSystem != occupiedPointSystem)
//		{
			FindNeighbours();
			FindNextLeft();
//			holder = lastOccupiedPointSystem;
			StoreLast();
			TransformableIslandCheck();
//		}
/*		if (posCheckLock || onceOff)
		{
			print ("check2");
			onceOff = false;
			lastOccupiedPointSystem = holder;
			FindNeighbours();
			FindNextLeft();
		}
*/		LayerTagging ();
	}

	public void FindOccupied()
	{
		// Cast Ray Down from Player to find Occupied PointSystem
		Ray ray = new Ray(this.gameObject.transform.position, Vector3.down);
		RaycastHit hit;
		Physics.Raycast (ray.origin, ray.direction, out hit, 0.75f, 1 << 10);
		if(debugOn)
			Debug.DrawRay (ray.origin, ray.direction, Color.black, Mathf.Infinity);

		// If Ray hits
		if (hit.collider != null) {

			// Stores Occupied PointSystem
			occupiedPointSystem = hit.collider.transform.parent.gameObject;

			// If the Occupied block has changed in the last frame
			if (occupiedPointSystem != null)
			{

				// Tags Occupied PointSystem and stores Collider
				occupiedPointSystem.tag = "Occupied";
				playerMovement.OccupiedPointSystemReference = occupiedPointSystem;

				foreach (Transform child in occupiedPointSystem.transform)
				{
					if (child.name == "Occupied Point")
						occupiedCollider = child.gameObject;
				}

				// Clears previous CastPoints and Shuffles new ones
				occupiedCastPointList.Clear ();
				foreach (Transform trans in occupiedPointSystem.transform) {
					if (trans.name == "Cast Point") {
						occupiedCastPointList.Add (trans.gameObject);
					}
				}
				if (occupiedPointSystem.transform.parent.name != "Exit")
					occupiedCastPointList = ShuffleCastPoints (occupiedCastPointList);
			}
		} else {
			TagAndSystemClear ();
		}
	}

	// Clears all Tags, nullifies all Systems, and resets all important variables
	void TagAndSystemClear()
	{
		occupiedCastPointList.Clear ();
		for (int i = 0; i < 4; i++) neighbourPointSystems[i] = null;

		idNext = -1;
		
		// Resets all Tags
		foreach(GameObject tagged in GameObject.FindGameObjectsWithTag("Occupied")) tagged.tag = "Untagged";
		playerMovement.OccupiedPointSystemReference = null;
		foreach(GameObject tagged in GameObject.FindGameObjectsWithTag("NextLeft")) tagged.tag = "Untagged";
		playerMovement.NextLeftPointSystemReference = null;
		foreach(GameObject tagged in GameObject.FindGameObjectsWithTag("LastOccupied")) tagged.tag = "Untagged";
		foreach(GameObject tagged in GameObject.FindGameObjectsWithTag("Neighbour")) tagged.tag = "Untagged";
	}

	// Shuffles CastPoints in the order of their Position, (+X, -Z, -X, +Z)
	public List<GameObject> ShuffleCastPoints(List<GameObject> list)
	{
		List<GameObject> newList = list.OrderBy(GameObject => GameObject.transform.position.x).ThenBy (GameObject => GameObject.transform.position.z).ToList();
		newList.Reverse ();
		GameObject temp;
		temp = newList [1];
		newList[1] = newList[3];
		newList [3] = temp;
		temp = newList [1];
		newList[1] = newList[2];
		newList [2] = temp;
		int i = 0;
		foreach (GameObject castPoint in newList)
		{
			// Tags CastPoints According to their position
			if(i == 0)
				castPoint.tag = "Pos X";
			if(i == 1)
				castPoint.tag = "Neg Z";
			if(i == 2)
				castPoint.tag = "Neg X";
			if(i == 3)
				castPoint.tag = "Pos Z";
			i++;
		}
		return newList;
	}

	void FindNeighbours()
	{
		// Untags all of the current Neighbours
		for (int j = 0; j < 4; j++)
		{
			if (neighbourPointSystems[j] != null)
			{
				neighbourPointSystems[j].tag = "Untagged";
				neighbourPointSystems[j] = null;
			}
		}

		// For each of the CastPoints in the Occupied PointSystem, a raycast will be made to find Negative Perspective Neighbours
		int i = 0;
		foreach (GameObject castPoint in occupiedCastPointList)
		{
			int coreRot = Mathf.RoundToInt (occupiedCollider.transform.parent.parent.rotation.eulerAngles.y);

			// Casts a ray in a direction depending on the Camera Position
			RaycastHit[] raycastHitsNegPers;
			Vector3 angle = (castPoint.transform.TransformDirection (rotateCam.CamQua * Vector3.forward));

			angle = Quaternion.Euler(0, -coreRot, 0) * angle;

			// Fix for rotate transformables
			if (occupiedCollider.transform.parent.parent.parent.name == "Rotate" && occupiedCollider.transform.parent.parent.name != "Ramp")
			{
				int rotRot = Mathf.RoundToInt (occupiedCollider.transform.parent.parent.parent.rotation.eulerAngles.y);
				angle = Quaternion.Euler(0, rotRot, 0) * angle;
			}

			Ray rayNegPers = new Ray(castPoint.transform.position, angle);
			raycastHitsNegPers = Physics.RaycastAll (rayNegPers, Mathf.Infinity,1<<8).OrderBy(h=>h.distance).ToArray();
			if(debugOn)
				Debug.DrawRay (rayNegPers.origin, rayNegPers.direction, Color.red, Mathf.Infinity);
			
			List <GameObject> raycastHitsNegPersList = new List<GameObject>();
			foreach(RaycastHit hit in raycastHitsNegPers)raycastHitsNegPersList.Add (hit.collider.gameObject);

			List <GameObject> raycastHitsNegPersDelList = new List<GameObject>();

			if (raycastHitsNegPersList.Count != 0 && raycastHitsNegPersList[0].name != "Block Point")
			{
				foreach (GameObject point in raycastHitsNegPersList)
				{
					if (!CastPointCheck (point.tag, castPoint.tag) || point.name == "Block Point")
					{
						raycastHitsNegPersDelList.Add (point);
					}
				}

				foreach (GameObject point in raycastHitsNegPersDelList)
				{
					raycastHitsNegPersList.Remove (point);
				}

				raycastHitsNegPersDelList.Clear ();
				
				// Checks to make sure is not a blocked move
				if (raycastHitsNegPersList.Count != 0)
				{
					neighbourPointSystems[i] = raycastHitsNegPersList[0].transform.parent.gameObject;

					if (neighbourPointSystems[i].transform.parent.name == "Fire")
					{
						if (neighbourPointSystems[i].GetComponent<SecondaryBlocks>().fireClear != true)
						{
							neighbourPointSystems[i] = null;
						}
					}

					raycastHitsNegPersList.Clear ();
				}
				else
				{
					neighbourPointSystems[i] = null;
				}
			}
			else
			{
				neighbourPointSystems[i] = null;
			}
			
			// For each of the CastPoints in the Occupied PointSystem, a raycast will be made to find Physical Neighbours

			// Casts a ray in all directions from the OccupiedCollider
			RaycastHit hitPhys;
			RaycastHit hitPhysRampUp;
			RaycastHit hitPhysRampDown;

			Vector3 vectorDir = new Vector3 (0f, 0f, 0f);
			if(i == 0)
				vectorDir = Vector3.right;
			if(i == 1)
				vectorDir = Vector3.back;
			if(i == 2)
				vectorDir = Vector3.left;
			if(i == 3)
				vectorDir = Vector3.forward;
			if (occupiedCollider.transform.parent.parent.name != "Ramp")
			{
				Physics.Raycast (occupiedCollider.transform.position, vectorDir, out hitPhys, 1f, 1 << 10);

				// Adds the collided object's PointSystem into the Neighbour Array
				if (hitPhys.collider != null && hitPhys.collider.tag != "Block")
				{
					neighbourPointSystems[i] = hitPhys.collider.transform.parent.gameObject;
				}

				if(debugOn)
					Debug.DrawRay (occupiedCollider.transform.position, vectorDir, Color.white, Mathf.Infinity);
			}
			else
			{
				Vector3 colUp = new Vector3 (occupiedCollider.transform.position.x, occupiedCollider.transform.position.y + 0.5f, occupiedCollider.transform.position.z);
				Vector3 colDown = new Vector3 (occupiedCollider.transform.position.x, occupiedCollider.transform.position.y - 0.5f, occupiedCollider.transform.position.z);

				Physics.Raycast (colUp, vectorDir, out hitPhysRampUp, 1f, 1 << 10);
				Physics.Raycast (colDown, vectorDir, out hitPhysRampDown, 1f, 1 << 10);

				if (hitPhysRampUp.collider != null && hitPhysRampUp.collider.tag != "Block")
				{
					neighbourPointSystems[i] = hitPhysRampUp.collider.transform.parent.gameObject;
				}
				
				if (hitPhysRampDown.collider != null && hitPhysRampDown.collider.tag != "Block")
				{
					neighbourPointSystems[i] = hitPhysRampDown.collider.transform.parent.gameObject;
				}

				if(debugOn)
					Debug.DrawRay (colUp, vectorDir, Color.white, Mathf.Infinity);
				if(debugOn)
					Debug.DrawRay (colDown, vectorDir, Color.white, Mathf.Infinity);
			}


			
			// For each of the CastPoints in the Occupied PointSystem, a raycast will be made to find Positive Perspective Neighbours

			// Casts a ray in a direction depending on the Camera Position
			RaycastHit[] raycastHitsPosPers;
			angle = (castPoint.transform.TransformDirection (rotateCam.CamQua * Vector3.back));

			angle = Quaternion.Euler(0, -coreRot, 0) * angle;

			// Fix for rotate transformables
			if (occupiedCollider.transform.parent.parent.parent.name == "Rotate" && occupiedCollider.transform.parent.parent.name != "Ramp")
			{
				int rotRot = Mathf.RoundToInt (occupiedCollider.transform.parent.parent.parent.rotation.eulerAngles.y);
				angle = Quaternion.Euler(0, rotRot, 0) * angle;
			}

			Ray rayPosPers = new Ray(castPoint.transform.position, angle);
			raycastHitsPosPers = Physics.RaycastAll (rayPosPers, Mathf.Infinity,1<<8).OrderBy(h=>h.distance).ToArray();
			if(debugOn)
				Debug.DrawRay (rayPosPers.origin, rayPosPers.direction, Color.blue, Mathf.Infinity);

			List <GameObject> raycastHitsPosPersList = new List<GameObject>();
			foreach(RaycastHit hit in raycastHitsPosPers)raycastHitsPosPersList.Add (hit.collider.gameObject);
		
			List <GameObject> raycastHitsPosPersDelList = new List<GameObject>();

			if (raycastHitsPosPersList.Count != 0 && raycastHitsPosPersList[0].name != "Block Point")
			{
				foreach (GameObject point in raycastHitsPosPersList)
				{
					if (!CastPointCheck (point.tag, castPoint.tag) || point.name == "Block Point")
					{
						raycastHitsPosPersDelList.Add (point);
					}
				}
				
				foreach (GameObject point in raycastHitsPosPersDelList)
				{
					raycastHitsPosPersList.Remove (point);
				}

				raycastHitsPosPersDelList.Clear ();
				
				// Checks to make sure is not a blocked move
				if (raycastHitsPosPersList.Count != 0)
				{
					neighbourPointSystems[i] = raycastHitsPosPersList[(raycastHitsPosPersList.Count-1)].transform.parent.gameObject;

					if (neighbourPointSystems[i].transform.parent.name == "Fire")
					{
						if (neighbourPointSystems[i].GetComponent<SecondaryBlocks>().fireClear != true)
						{
							neighbourPointSystems[i] = null;
						}
					}

					raycastHitsPosPersList.Clear ();
				}
			}

			i++;
		}

		// Tags all of the current Neighbours as Neighbour
		foreach (GameObject neighbour in neighbourPointSystems)
		{
			if (neighbour != null && neighbour.tag != "NextLeft")
				neighbour.tag = "Neighbour";
		}

		foreach (GameObject loTag in GameObject.FindGameObjectsWithTag ("LastOccupied"))
			loTag.tag = "Untagged";

		if (lastOccupiedPointSystem != occupiedPointSystem)
		{
			if (lastOccupiedPointSystem != null)
				lastOccupiedPointSystem.tag = "LastOccupied";
			occupiedPointSystem.tag = "Occupied";
			playerMovement.OccupiedPointSystemReference = occupiedPointSystem;
		}
	}

	// Checks to make sure that CastPoints collide with opposite side
	bool CastPointCheck (string tag1, string tag2)
	{
		if (
			(tag1 == "Pos X" && tag2 == "Neg X") ||
			(tag1 == "Neg Z" && tag2 == "Pos Z") ||
			(tag1 == "Neg X" && tag2 == "Pos X") ||
			(tag1 == "Pos Z" && tag2 == "Neg Z")
			)
			return true;
		else
			return false;
	}

	void FindNextLeft()
	{
		idNext = -1;
		// On beginning move
		if (lastOccupiedPointSystem == null)
		{
			for (int i = 4; i --> 0;)
			{
				if (neighbourPointSystems [i] != null)
				{
					idNext = i;
				}
			}
		}
		else
		{

			// Cycles through all potential Neighbours anti-clockwise from +Z to find the left-most
			for (int i = 4; i --> 0;) {
				// If there is a neighbour, update idNext
				if (neighbourPointSystems [i] != null) {
					if (neighbourPointSystems [i] == lastOccupiedPointSystem)
						idLast = i;
//					else if (lastOccupiedPointSystem = occupiedPointSystem)
//						idLast = 4;
				}
			}

			// If all Neighbours are null
			if ((neighbourPointSystems [0] == null) &&
				(neighbourPointSystems [1] == null) &&
				(neighbourPointSystems [2] == null) &&
				(neighbourPointSystems [3] == null)) {
				idLast = -1;
				idNext = -1;
			}

			switch (idLast) {
			case 0:
				if (neighbourPointSystems [1] != null)
					idNext = 1;
				else if (neighbourPointSystems [2] != null)
					idNext = 2;
				else if (neighbourPointSystems [3] != null)
					idNext = 3;
				else if (neighbourPointSystems [0] != null)
					idNext = 0;
				else
				{
					idNext = -1;
				}
				break;
			
			case 1:
				if (neighbourPointSystems [2] != null)
					idNext = 2;
				else if (neighbourPointSystems [3] != null)
					idNext = 3;
				else if (neighbourPointSystems [0] != null)
					idNext = 0;
				else if (neighbourPointSystems [1] != null)
					idNext = 1;
				else
				{
					idNext = -1;
				}
				break;
			
			case 2:
				if (neighbourPointSystems [3] != null)
					idNext = 3;
				else if (neighbourPointSystems [0] != null)
					idNext = 0;
				else if (neighbourPointSystems [1] != null)
					idNext = 1;
				else if (neighbourPointSystems [2] != null)
					idNext = 2;
				else
				{
					idNext = -1;
				}
				break;
			
			case 3:
				if (neighbourPointSystems [0] != null)
					idNext = 0;
				else if (neighbourPointSystems [1] != null)
					idNext = 1;
				else if (neighbourPointSystems [2] != null)
					idNext = 2;
				else if (neighbourPointSystems [3] != null)
					idNext = 3;
				else
				{
					idNext = -1;
				}
				break;

			case -1:
				if (neighbourPointSystems [0] != null)
					idNext = 0;
				else if (neighbourPointSystems [1] != null)
					idNext = 1;
				else if (neighbourPointSystems [2] != null)
					idNext = 2;
				else if (neighbourPointSystems [3] != null)
					idNext = 3;
				else
				{
					idNext = -1;
				}
				break;
		
			default:
				idLast = -1;
				idNext = -1;
				break;
			}
		}
		if (idNext != -1)
		{
			neighbourPointSystems[idNext].tag = "NextLeft";
			playerMovement.NextLeftPointSystemReference = neighbourPointSystems[idNext];
		}
	}

	void StoreLast()
	{
		if (lastOccupiedPointSystem != occupiedPointSystem && occupiedPointSystem != null)
		{
			lastOccupiedPointSystem = occupiedPointSystem;
		}
	}

	void LayerTagging()
	{
		// objects behind player depth
		// neighbours and occupied
		// player
		// objects in front of player depth
	}

	// Debugging Function for visually displaying Occupied, Neighbour, and NextLeft blocks;
	void ColourNeighbours()
	{

		Renderer rend;
		Transform level = GameObject.Find ("Level").transform;

		// Un-colour all Paths
		foreach (Transform cube in level)
		{
			if (cube.gameObject.name == "Path")
			{
				rend = cube.gameObject.GetComponent<Renderer>();
				rend.material = materialUntagged;
			}
		}
		foreach (Transform cube in level)
		{
			foreach (Transform path in cube)
			{
				if (path.name != "SpriteManager")
				{
					rend = path.gameObject.GetComponent<Renderer>();
					rend.material = materialUntagged;
				}
			}
		}

		// Colour Neighbours
		GameObject[] neighbourSystems = GameObject.FindGameObjectsWithTag("Neighbour");
		foreach (GameObject neighbourSystem in neighbourSystems)
		{
			rend = neighbourSystem.GetComponentInParent<Renderer> ();
			rend.material = materialNeighbour;
		}

		//Colour NextLeft
		GameObject nextLeft = GameObject.FindGameObjectWithTag ("NextLeft");
		if (nextLeft != null)
		{
			rend = nextLeft.GetComponentInParent<Renderer>();
			rend.material = materialNextLeft;
		}

		//Colour LastOccupied
		GameObject[] lastOccupiedSystems = GameObject.FindGameObjectsWithTag("LastOccupied");
		foreach (GameObject lastOccupiedSystem in lastOccupiedSystems)
		{
			rend = lastOccupiedSystem.GetComponentInParent<Renderer> ();
			rend.material = materialLastOccupied;
		}
		
		//Colour Occupied
		GameObject occupied = GameObject.FindGameObjectWithTag ("Occupied");
		if (occupied != null)
		{
			rend = occupied.GetComponentInParent<Renderer> ();
			rend.material = materialOccupied;
		}
	}

	void TransformableIslandCheck()
	{
		if (temp != null)
		{
			temp.Check = false;
		}
		GameObject nextLeft = GameObject.FindGameObjectWithTag ("NextLeft");
		if (nextLeft != null && nextLeft.transform.parent.parent.gameObject != null)
		{
			nextLeft = nextLeft.transform.parent.parent.gameObject;
			if (nextLeft.tag == "TransformableIsland")
			{
				tiScript = nextLeft.GetComponent<TransformableIsland>();
				tiScript.Check = true;
			}
		}
		GameObject occupied = GameObject.FindGameObjectWithTag ("Occupied");
		if (occupied != null && occupied.transform.parent.parent.gameObject != null)
		{
				occupied = occupied.transform.parent.parent.gameObject;
			if (occupied.tag == "TransformableIsland")
			{
				tiScript = occupied.GetComponent<TransformableIsland>();
				temp = tiScript;
				tiScript.Check = true;
			}
		}
	}
}