﻿using UnityEngine;
using System.Collections;

public class Buttons : MonoBehaviour 
{
	public GameObject worldSelectionScreen;
	public GameObject startScreen;
	public GameObject world0;
	public GameObject world1;
	public GameObject world2;
	public GameObject world3;
	public GameObject world4;
	public GameObject world5;
	public GameObject settingsScreen;
	public GameObject loadingScreen;
	public GameObject exitWindow;

	public GameObject pauseWindow;
	public GameObject canvas;
	public GameObject resumeButton;
	public GameObject quitButton;
	public GameObject pauseButton;
	public GameObject slider;



	public void Start()
	{
		if (pauseWindow != null)
		pauseWindow.SetActive(false);

	}

	public void StartButton()
	{
		Application.LoadLevel("Tutorial_Screen");
	}

	public void QuitApplication()
	{
		StartCoroutine (QuitApplication_Delay());
	}

	public IEnumerator QuitApplication_Delay()
	{
		yield return new WaitForSeconds (0.25f);
		Application.Quit();
	}

	public void PauseButton()
	{
		if(pauseWindow != null)
		{
			pauseWindow.SetActive(true);
		}
	}


	public void ResumeButton()
	{
		if (pauseWindow != null)
		pauseWindow.SetActive(false);
		//slider.SetActive(true);
		//pauseButton.SetActive(true);
	}

	public void QuitButton()
	{
		Application.LoadLevel("Start_Screen");
	}


	public void ExitLevel()
	{
		Application.LoadLevel("Tutorial_Screen");
	}

	public void LoadLevel1()
	{
		Application.LoadLevel("Level_1");
	}

	public void LoadLevel2()
	{
		Application.LoadLevel("Level_2");
	}

	public void LoadLevel3()
	{
		Application.LoadLevel("Level_3");
	}

	public void LoadStartScreen()
	{
		Application.LoadLevel("Start_Screen");
	}

	public GameObject ui_QuitPopUp;
	public GameObject ui_PauseLevelPopUp;

	
	// --------------
	// NEW UI BUTTONS
	// --------------


	// MAIN SCREEN
	
	public void UI_MainScreen_Quit() 
	{
		StartCoroutine (MainScreen_Quit_Delay ());
	}

	IEnumerator MainScreen_Quit_Delay( )
	{
		yield return new WaitForSeconds (0.25f);
		DisableUi ();
		ui_QuitPopUp.SetActive (true);
	}

	public void UI_MainScreen_QuitYes()
	{
		StartCoroutine (UI_MainScreen_QuiYes_Delay());
	}

	public IEnumerator UI_MainScreen_QuiYes_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		Application.Quit ();
	}

	public void UI_MainScreen_QuitNo() {ui_QuitPopUp.SetActive (false);}

	public void UI_MainScreen_Play() 
	{
		StartCoroutine (MainScreen_Play_Delay ());
	}

	IEnumerator MainScreen_Play_Delay( )
	{
		yield return new WaitForSeconds (0.25f);
		DisableUi ();
		worldSelectionScreen.SetActive (true);
	}

	public void UI_MainScreen_Settings() 
	{
		StartCoroutine (MainScreen_Settings_Delay());
	}

	IEnumerator MainScreen_Settings_Delay( )
	{
		yield return new WaitForSeconds (0.25f);
		DisableUi ();
		settingsScreen.SetActive (true);
	}

	// SETTINGS SCREEN

	public void UI_ReturnToMainScreen() 
	{
		StartCoroutine (UI_ReturnToMainScreen_Delay());
	}

	public IEnumerator UI_ReturnToMainScreen_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		startScreen.SetActive (true);
	}

	// WORLD SELECTS
	public void DisableUi()
	{
		world0.SetActive (false);
		world1.SetActive (false);
		world2.SetActive (false);
		world3.SetActive (false);
		world4.SetActive (false);
		world5.SetActive (false);
		worldSelectionScreen.SetActive (false);
		startScreen.SetActive (false);
		settingsScreen.SetActive (false);
		exitWindow.SetActive (false);
		loadingScreen.SetActive (false);
	}

	public void UI_WorldSelect_0() 
	{
		StartCoroutine (UI_WorldSelect_0_Delay ());
	}

	public IEnumerator UI_WorldSelect_0_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world0.SetActive (true);
	}

	public void UI_WorldSelect_1() 
	{
		StartCoroutine (UI_WorldSelect_1_Delay ()); 
	}

	public IEnumerator UI_WorldSelect_1_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world1.SetActive (true);
	}

	public void UI_WorldSelect_2() 
	{
		StartCoroutine (UI_WorldSelect_2_Delay ());
	}

	public IEnumerator UI_WorldSelect_2_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world2.SetActive (true);
	}

	public void UI_WorldSelect_3() 
	{
		StartCoroutine (UI_WorldSelect_3_Delay ());
	}

	public IEnumerator UI_WorldSelect_3_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world3.SetActive (true);
	}

	public void UI_WorldSelect_4() 
	{
		StartCoroutine (UI_WorldSelect_4_Delay ());
	}

	public IEnumerator UI_WorldSelect_4_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world4.SetActive (true);
	}

	public void UI_WorldSelect_5() 
	{
		StartCoroutine (UI_WorldSelect_5_Delay ());
	}

	public IEnumerator UI_WorldSelect_5_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		world5.SetActive (true);
	}

	// LEVEL SELECTS

	public void UI_LevelSelect_0_1() 
	{
		StartCoroutine (UI_LevelSelect_0_1_Delay ());
	}

	public IEnumerator UI_LevelSelect_0_1_Delay() 
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_1");
	}

	public void UI_LevelSelect_0_2() 
	{
		StartCoroutine (UI_LevelSelect_0_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_0_2_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_2");
	}
	

	public void UI_LevelSelect_0_3()
	{
		StartCoroutine (UI_LevelSelect_0_3_Delay ());
	}

	public IEnumerator UI_LevelSelect_0_3_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_3");
	}


	public void UI_LevelSelect_1_1()
	{
		StartCoroutine (UI_LevelSelect_1_1_Delay ());
	}

	public IEnumerator UI_LevelSelect_1_1_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_4");
	}

	public void UI_LevelSelect_1_2()
	{
		StartCoroutine (UI_LevelSelect_1_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_1_2_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_5");
	}
	
	public void UI_LevelSelect_1_3()
	{
		StartCoroutine (UI_LevelSelect_1_3_Delay());
	}

	public IEnumerator UI_LevelSelect_1_3_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_6");
	}

	public void UI_LevelSelect_1_4()
	{
		StartCoroutine (UI_LevelSelect_1_4_Delay ());
	}

	public IEnumerator UI_LevelSelect_1_4_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_7");
	}

	public void UI_LevelSelect_1_5()
	{
		StartCoroutine (UI_LevelSelect_1_5_Delay());
	}

	public IEnumerator UI_LevelSelect_1_5_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_8");
	}
	
	public void UI_LevelSelect_2_1()
	{
		StartCoroutine (UI_LevelSelect_2_1_Delay());
	}

	public IEnumerator UI_LevelSelect_2_1_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_9");
	}

	public void UI_LevelSelect_2_2()
	{
		StartCoroutine (UI_LevelSelect_2_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_2_2_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_10");
	}

	public void UI_LevelSelect_2_3()
	{
		StartCoroutine (UI_LevelSelect_2_3_Delay ());
	}

	public IEnumerator UI_LevelSelect_2_3_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_11");
	}

	public void UI_LevelSelect_2_4()
	{
		StartCoroutine (UI_LevelSelect_2_4_Delay ());
	}

	public IEnumerator UI_LevelSelect_2_4_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_12");
	}

	public void UI_LevelSelect_2_5()
	{
		StartCoroutine (UI_LevelSelect_2_5_Delay ());
	}

	public IEnumerator UI_LevelSelect_2_5_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_13");
	}

	public void UI_LevelSelect_3_1() 
	{
		StartCoroutine (UI_LevelSelect_3_1_Delay ());
	}

	public IEnumerator UI_LevelSelect_3_1_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_14");
	}

	public void UI_LevelSelect_3_2()
	{
		StartCoroutine (UI_LevelSelect_3_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_3_2_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_15");
	}

	public void UI_LevelSelect_3_3()
	{
		StartCoroutine (UI_LevelSelect_3_3_Delay ());
	}

	public IEnumerator UI_LevelSelect_3_3_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_16");
	}

	public void UI_LevelSelect_3_4()
	{
		StartCoroutine (UI_LevelSelect_3_4_Delay ());
	}

	public IEnumerator UI_LevelSelect_3_4_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_17");
	}

	public void UI_LevelSelect_3_5()
	{
		StartCoroutine (UI_LevelSelect_3_5_Delay ());
	}

	public IEnumerator UI_LevelSelect_3_5_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_18");
	}
	
	public void UI_LevelSelect_4_1()
	{
		StartCoroutine (UI_LevelSelect_4_1_Delay ());
	}

	public IEnumerator UI_LevelSelect_4_1_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_19");
	}

	public void UI_LevelSelect_4_2()
	{
		StartCoroutine (UI_LevelSelect_4_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_4_2_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_20");
	}
	
	public void UI_LevelSelect_4_3()
	{
		StartCoroutine (UI_LevelSelect_4_3_Delay ());
	}

	public IEnumerator UI_LevelSelect_4_3_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_21");
	}
	
	public void UI_LevelSelect_4_4()
	{
		StartCoroutine (UI_LevelSelect_4_4_Delay ());
	}

	public IEnumerator UI_LevelSelect_4_4_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_22");
	}

	public void UI_LevelSelect_4_5()
	{
		StartCoroutine (UI_LevelSelect_4_5_Delay ());
	}

	public IEnumerator UI_LevelSelect_4_5_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_23");
	}
	
	public void UI_LevelSelect_5_1()
	{
		StartCoroutine (UI_LevelSelect_5_1_Delay ());
	}

	public IEnumerator UI_LevelSelect_5_1_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_24");
	}

	public void UI_LevelSelect_5_2()
	{
		StartCoroutine (UI_LevelSelect_5_2_Delay ());
	}

	public IEnumerator UI_LevelSelect_5_2_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_25");
	}

	public void UI_LevelSelect_5_3()
	{
		StartCoroutine (UI_LevelSelect_5_3_Delay ());
	}

	public IEnumerator UI_LevelSelect_5_3_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_26");
	}

	public void UI_LevelSelect_5_4()
	{
		StartCoroutine (UI_LevelSelect_5_4_Delay ());
	}

	public IEnumerator UI_LevelSelect_5_4_Delay ()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_27");
	}

	public void UI_LevelSelect_5_5()
	{
		StartCoroutine (UI_LevelSelect_5_5_Delay ());
	}

	public IEnumerator UI_LevelSelect_5_5_Delay()
	{
		yield return new WaitForSeconds (0.2f);
		DisableUi ();
		loadingScreen.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel("Level_28");
	}
	
	// IN GAME

	public void UI_PauseLevel()
	{
		StartCoroutine (UI_PauseLevel_Delay());
	}

	public IEnumerator UI_PauseLevel_Delay()
	{
		yield return new WaitForSeconds (0.3f);
		Time.timeScale = 0.0f;
		ui_PauseLevelPopUp.SetActive (true);
	}

	public void UI_ResumeLevel()
	{
		StartCoroutine (UI_ResumeLevel_Delay());
		ui_PauseLevelPopUp.SetActive (false);
		Time.timeScale = 1.0f;
	}

	public IEnumerator UI_ResumeLevel_Delay()
	{
		yield return new WaitForSeconds (0.2f);
	}

	public void UI_QuitLevel()
	{
		StartCoroutine (UI_QuitLevel_Delay());
	}

	public IEnumerator UI_QuitLevel_Delay()
	{

		for (float i = 0; i < 0.2f; i += Time.fixedDeltaTime)
			yield return null;
		Time.timeScale = 1.0f;
		Application.LoadLevel("Start_Scene");
	}	
}
