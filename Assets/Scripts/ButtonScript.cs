﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour
{
	private bool buttonOn = false;
	
	public GameObject fireAndFan;
	public GameObject exitModel;
	private GameObject buttonPlatform;
	public Material buttonMaterialOn;
	public Material buttonMaterialOff;

	void Start()
	{
		buttonPlatform = this.gameObject;
		Renderer rend = buttonPlatform.GetComponent<Renderer>();
		rend.material = buttonMaterialOff;
	}

	public void ButtonPress()
	{
		buttonOn = !buttonOn;

		if (buttonPlatform != null && buttonMaterialOn != null && buttonMaterialOff != null)
		{
			if (buttonOn)
			{
				Renderer rend = buttonPlatform.GetComponent<Renderer>();
				rend.material = buttonMaterialOn;
			}
			else
			{
				Renderer rend = buttonPlatform.GetComponent<Renderer>();
				rend.material = buttonMaterialOff;
			}		
		}

		if (fireAndFan != null)
		{
			if (buttonOn)
				fireAndFan.GetComponent<FireAndFan>().SwitchOn();
			else
				fireAndFan.GetComponent<FireAndFan>().SwitchOff();
		}
		
		if (exitModel != null)
		{
			if (buttonOn)
				exitModel.GetComponent<ExitButtons>().SwitchOn();
			else
				exitModel.GetComponent<ExitButtons>().SwitchOff();
		}
	}
}
