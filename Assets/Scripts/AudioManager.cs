using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour 
{

	public static AudioManager instance;

	void Awake()
	{
		instance = this;
	}
	
	public AudioMixerGroup sfxMixer;
	public AudioMixerGroup musicMixer;
	public AudioMixer masterMixer;

	public AudioClip Acoustic1;
	public AudioClip Acoustic2;
	public AudioClip Acoustic3;
	public AudioClip Acoustic4;
	public AudioClip beep1;
	public AudioClip beep2;
	public AudioClip blockman;
	public AudioClip button;
	public AudioClip buttonBeep;
	public AudioClip buzzing1;
	public AudioClip computerStartUp1;
	public AudioClip computerStartUp2;
	public AudioClip crackle1;
	public AudioClip crackle2;
	public AudioClip crackle3;
	public AudioClip deviceRemoved;
	public AudioClip dialUp;
	public AudioClip discFan1;
	public AudioClip discFan2;
	public AudioClip error1;
	public AudioClip error2;
	public AudioClip electrodoodle;
	public AudioClip fan1;
	public AudioClip fan2;
	public AudioClip fan3;
	public AudioClip fan4;
	public AudioClip fanSwitch;
	public AudioClip fileDelete;
	public AudioClip generator;
	public AudioClip glitch;
	public AudioClip hangUp;
	public AudioClip keyboardTyping;
	public AudioClip lightFlicker1;
	public AudioClip mouseClick1;
	public AudioClip mouseClick2;
	public AudioClip pamgaea;
	public AudioClip powercore;
	public AudioClip powerUp;
	public AudioClip shutDown;
	public AudioClip spark1;
	public AudioClip spark2;
	public AudioClip spark3;
	public AudioClip startUp;
	public AudioClip discharge1;
	public AudioClip discharge2;
	public AudioClip wind;
	public AudioClip woosh1;
	public AudioClip woosh2;
	public AudioClip woosh3;

	public GameObject muteMusic;
	public GameObject unMuteMusic;
	public GameObject muteSound;
	public GameObject unMuteSound;

	// Use this for initialization
	void Start () 
	{
		print ("Start");
		float sfxVol = PlayerPrefs.GetFloat ("SfxVolume",0f);
		//masterMixer.SetFloat ("SfxVolume", sfxVol);
		if (sfxVol == 0)
			MuteSFX ();
		else
			UnMuteSFX ();

		float musicVol = PlayerPrefs.GetFloat ("MusicVolume",0f);
		if (musicVol == 0)
			MuteMusic ();
		else
			UnMuteMusic ();

		//masterMixer.SetFloat ("MusicVolume", musicVol);
		LevelMusic ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void PlaySFXClip(AudioClip clip, AudioMixerGroup mixer)
	{
		GameObject newAudioClip = Instantiate(Resources.Load("Audioclip")) as GameObject;

		newAudioClip.GetComponent<AudioSource> ().clip = clip;
		newAudioClip.GetComponent<AudioSource> ().outputAudioMixerGroup = mixer;
		newAudioClip.GetComponent<AudioSource> ().Play ();

//		Destroy (newAudioClip, newAudioClip.GetComponent<AudioSource> ().clip.length + 0.5f);

	}
	public void PlaySFXClip(AudioClip clip, AudioMixerGroup mixer, bool loop)
	{
		GameObject newAudioClip = Instantiate(Resources.Load("Audioclip")) as GameObject;
		
		newAudioClip.GetComponent<AudioSource> ().clip = clip;
		newAudioClip.GetComponent<AudioSource> ().loop = loop;
		newAudioClip.GetComponent<AudioSource> ().outputAudioMixerGroup = mixer;
		newAudioClip.GetComponent<AudioSource> ().Play ();
		
//		Destroy (newAudioClip, newAudioClip.GetComponent<AudioSource> ().clip.length + 0.5f);
		
	}
	public void MuteSFX()
	{
		masterMixer.SetFloat ("SfxVolume", 0f);
		PlayerPrefs.SetFloat ("SfxVolume", 0f);

		if (unMuteMusic != null) 
		{
			muteSound.SetActive(false);
			unMuteSound.SetActive(true);
		}
	}
	public void UnMuteSFX()
	{
		masterMixer.SetFloat ("SfxVolume", -80f);
		PlayerPrefs.SetFloat ("SfxVolume", -80f);
		if (unMuteMusic != null) 
		{
			muteSound.SetActive(true);
			unMuteSound.SetActive(false);
		}
	}

	public void MuteMusic()
	{
		masterMixer.SetFloat ("MusicVolume", 0f);
		PlayerPrefs.SetFloat ("MusicVolume", 0f);
		if (unMuteMusic != null) 
		{
			muteMusic.SetActive(false);
			unMuteMusic.SetActive(true);
		}
	}
	public void UnMuteMusic()
	{
		masterMixer.SetFloat ("MusicVolume", -80f);
		PlayerPrefs.SetFloat ("MusicVolume", -80f);
		if (unMuteMusic != null) 
		{
			muteMusic.SetActive(true);
			unMuteMusic.SetActive(false);
		}
	}

	public void LevelMusic()
	{
		int level = PlayerPrefs.GetInt ("CurrentLevel", 1);

		print ("Call debug");
		if (level >= 1 && level <= 3) 
		{
			if (AudioManager.instance != null) 
			{
				print ("Play Acoustin 1");
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic1, AudioManager.instance.musicMixer, true);
			}
		}

		else if (level > 3 && level <= 8) 
		{
			if (AudioManager.instance != null) 
			{
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic1, AudioManager.instance.musicMixer, true);
			}
		}

		else if (level > 8 && level <= 13) 
		{
			if (AudioManager.instance != null) 
			{
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic2, AudioManager.instance.musicMixer, true);
			}
		}

		else if (level > 13 && level <= 18) 
		{
			if (AudioManager.instance != null) 
			{
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic3, AudioManager.instance.musicMixer, true);
			}
		}

		else if (level > 18 && level <= 23) 
		{
			if (AudioManager.instance != null) 
			{
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic3, AudioManager.instance.musicMixer, true);
			}
		}

		else if (level > 23 && level <= 28) 
		{
			if (AudioManager.instance != null) 
			{
				AudioManager.instance.PlaySFXClip(AudioManager.instance.Acoustic4, AudioManager.instance.musicMixer, true);
			}
		}
	}
}

