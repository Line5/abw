﻿using UnityEngine;
using System.Collections;

public class ColorChange : MonoBehaviour 
{
	private Material _windParticles;
	public Color windColor;


	// Use this for initialization
	void Start ()
	{
		_windParticles = GetComponent<Renderer> ().material;

	}
	
	// Update is called once per frame
	void Update () 
	{
		_windParticles.SetColor ("_TintColor", windColor);

	}
}
