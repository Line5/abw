﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class PlayerMovement : MonoBehaviour
{
	private bool levelStart = false;
	private GameObject occupiedPointSystem;
	private GameObject nextLeftPointSystem;
	
	private GameObject occupiedPointSystemReference;
	private GameObject nextLeftPointSystemReference;
	
	private GameObject occupiedWalkPoint;
	private GameObject nextLeftWalkPoint;
	
	public GameObject fadeQuad;
	
	private RotateCam rotateCam;
	private NeighbourLeftSystem neighbourLeftSystem;
	
	public Camera cameraMain;
	public Camera cameraPlayer;
	public LayerMask maskOn;
	public LayerMask maskOff;
	
	public bool easyEase = false;
	
	private bool once = true;
	
	private bool buttonOnce = true;
	private bool speedOnce = true;
	
	public float speed = 20;
	public float speedMaxMultiplier = 2;
	public Slider speedSlider;
	public float speedAccuracy = 3;
	private float characterSpeed;
	private float minSpeed;
	
	private bool notMoving = true;
	private bool flag = true;
	
	private int idNextTemp = -1;
	
	private bool playerStopped;
	
	private bool deadActive;
	
	private bool boosted = false;
	private bool boostPlay = false;
	
	private bool teleportStarted = false;
	private int teleportFromID;
	
	private float initialSpeed;
	int m_frameCounter = 0;
	float m_timeCounter = 0.0f;
	float m_lastFramerate = 0.0f;
	float m_refreshTime = 0.5f;
	
	private bool rotatingToLook = false;
	
	
	public bool PlayerStopped
	{
		get
		{
			return playerStopped;
		}
		set
		{
			playerStopped = value;
		}
	}
	
	public bool DeadActive
	{
		get
		{
			return deadActive;
		}
	}
	
	public bool NotMoving
	{
		get
		{
			return notMoving;
		}
		set
		{
			notMoving = value;
		}
	}
	
	public GameObject OccupiedPointSystemReference
	{
		get
		{
			return occupiedPointSystemReference;
		}
		set
		{
			occupiedPointSystemReference = value;
		}
	}
	
	public GameObject NextLeftPointSystemReference
	{
		get
		{
			return nextLeftPointSystemReference;
		}
		set
		{
			nextLeftPointSystemReference = value;
		}
	}
	
	void Awake ()
	{
		fadeQuad.GetComponent<Renderer> ().material.color = Color.black;
		easyEase = false;

		speedSlider.value = 0.11112f;
	}
	
	void Start ()
	{
		rotateCam = GameObject.Find ("Camera").GetComponent<RotateCam> ();
		neighbourLeftSystem = this.gameObject.GetComponent<NeighbourLeftSystem> ();
		if (speed != 0)
			characterSpeed = 1 / speed;
		
		initialSpeed = speed;
	}
	
	void Update ()
	{
		if (!levelStart)
		{
			StartCoroutine (StartLevel ());
		}
		
		if (levelStart)
		{
			Movement ();
			UpdateSlider ();
			RenderAbove ();
		}
		if (occupiedPointSystem != null)
		{
			if (neighbourLeftSystem.IdNext != idNextTemp && occupiedPointSystem.transform.parent.name != "Exit")
			{
				StartCoroutine (RotateTo(neighbourLeftSystem.IdNext));
				idNextTemp = neighbourLeftSystem.IdNext;
			}
			
			if (occupiedPointSystem.transform.parent.name == "Exit")
			{
				GameObject lookPoint = null;
				foreach (Transform transform in occupiedPointSystem.transform)
					if (transform.name == "Exit Look Point")
						lookPoint = transform.gameObject;
				if (lookPoint != null && !rotatingToLook)
				{
					StartCoroutine (RotateToLook(lookPoint));
				}
			}
		}
	}
	
	void Movement()
	{
		
		occupiedPointSystem = occupiedPointSystemReference;
		nextLeftPointSystem = nextLeftPointSystemReference;
		
		if (occupiedPointSystem != null && nextLeftPointSystem != null)
		{
			foreach (Transform child in occupiedPointSystem.transform)
				if (child.name == "Walk Point")
					occupiedWalkPoint = child.gameObject;
			
			foreach (Transform child in nextLeftPointSystem.transform)
				if (child.name == "Walk Point")
					nextLeftWalkPoint = child.gameObject;
			
			if ((this.transform.position != occupiedWalkPoint.transform.position) && flag)
			{
				this.transform.position = occupiedWalkPoint.transform.position;
				flag = false;
			}
			
			if (((this.transform.position == occupiedWalkPoint.transform.position && characterSpeed != 0) && notMoving) && !playerStopped)
			{
				if(occupiedWalkPoint.transform.parent.parent.name == "Button")
				{
					occupiedWalkPoint.transform.parent.parent.GetComponent<ButtonScript>().ButtonPress ();
					
					if (buttonOnce)
					{
						AudioManager.instance.PlaySFXClip(AudioManager.instance.fanSwitch,AudioManager.instance.sfxMixer);
						buttonOnce = false;
					}
					
				}
				else
				{
					buttonOnce = true;
				}
				
				if(occupiedWalkPoint.transform.parent.parent.name == "Speed")
				{
					boostPlay = true;
					if (speedOnce)
					{
						//						AudioManager.instance.PlaySFXClip(AudioManager.instance.crackle1,AudioManager.instance.sfxMixer);
						speedOnce = false;
					}
				}
				
				if (occupiedWalkPoint.transform.parent.parent.name != "Exit" && occupiedWalkPoint.transform.parent.parent.name != "Teleport")
					StartCoroutine (MoveFromTo (occupiedWalkPoint.transform, nextLeftWalkPoint.transform));
				
				if (occupiedWalkPoint.transform.parent.parent.name == "Teleport" && !teleportStarted)
				{
					teleportFromID = occupiedWalkPoint.GetInstanceID ();
					teleportStarted = true;
					StartCoroutine (TeleportTo(occupiedWalkPoint.transform.parent.parent.gameObject));
				}
				
				else if (occupiedWalkPoint.transform.parent.parent.name == "Teleport" && (occupiedWalkPoint.GetInstanceID () != teleportFromID))
				{
					StartCoroutine (MoveFromTo (occupiedWalkPoint.transform, nextLeftWalkPoint.transform));
				}
			}
			if ((occupiedWalkPoint.transform.parent.parent.name == "Dead90" || nextLeftWalkPoint.transform.parent.parent.name == "Dead90") ||
			    (occupiedWalkPoint.transform.parent.parent.name == "Dead180" || nextLeftWalkPoint.transform.parent.parent.name == "Dead180"))
			{
				deadActive = true;
			}
			else
				deadActive = false;
			if (occupiedWalkPoint.transform.parent.parent.name == "Exit")
			{
				StartCoroutine (FinishLevel ());
			}
		}
	}
	
	void FrameRateAdjustSpeed()
	{
		if( m_timeCounter < m_refreshTime )
		{
			m_timeCounter += Time.deltaTime;
			m_frameCounter++;
		}
		else
		{
			m_lastFramerate = (float)m_frameCounter/m_timeCounter;
			m_frameCounter = 0;
			m_timeCounter = 0.0f;
		}
		
		if (m_lastFramerate > 10)
			speed = initialSpeed * (60 / m_lastFramerate);
		
		speed = speed * speedSlider.value;
	}
	
	public GameObject playAnim;
	
	void UpdateSlider()
	{
		//	FrameRateAdjustSpeed ();
		
		if (speedSlider.value < 1)
			speedSlider.value = 1;
		
		if (!boosted)
		{
			speedSlider.minValue = 0.5f;
			speedSlider.maxValue = speedMaxMultiplier;
		}
		if (boostPlay && !boosted)
		{
			StartCoroutine (StaticSpeedBoost ());
		}
		
		speed = initialSpeed * speedSlider.value;
		
		if (speedSlider.value <= (speedSlider.maxValue / 3))
		{
			playAnim.GetComponent<Animator> ().SetTrigger ("Normal Walk Cycle");
		}
		else if (speedSlider.value <= (speedSlider.maxValue / 3) * 2)
		{
			playAnim.GetComponent<Animator> ().SetTrigger ("Medium Walk Cycle");
		}
		else if (speedSlider.value <= (speedSlider.maxValue / 3) * 3)
		{
			playAnim.GetComponent<Animator> ().SetTrigger ("Fast Walk Cycle");
		}
	}
	
	public void OnUpdateSlider()
	{
		speedSlider.value = Mathf.Round (speedSlider.value);
		if (speedSlider.value == 0)
			speedSlider.value = 1;
	}
	
	public GameObject speedLogo;
	public Color speedColour;
	
	IEnumerator StaticSpeedBoost()
	{
		boosted = true;
		//		AudioManager.instance.PlaySFXClip(AudioManager.instance.crackle1,AudioManager.instance.sfxMixer);
		
		float speedStore = speedSlider.value;
		speedSlider.value = speedSlider.maxValue;
		speedLogo.GetComponent<Image> ().color = speedColour;
		speedSlider.enabled = false;
		yield return new WaitForSeconds(2f);
		speedSlider.enabled = true;
		speedLogo.GetComponent<Image> ().color = Color.white;
		speedSlider.value = speedStore;
		boostPlay = false;
		boosted = false;
		speedOnce = true;
	}
	
	IEnumerator TeleportTo(GameObject recieverGO)
	{
		TeleportScript teleportScript = recieverGO.GetComponent<TeleportScript> ();
		
		if (teleportScript.RecieveTeleportPointSystem != null)
		{
			yield return new WaitForSeconds(0.5f);
			teleportFromID = 0;
			
			foreach (Transform child in teleportScript.RecieveTeleportPointSystem.transform)
			{
				if (child.name == "Walk Point")
					this.transform.position = child.position;
			}
			neighbourLeftSystem.ReCall ();
		}
		else
			StartCoroutine (MoveFromTo (occupiedWalkPoint.transform, nextLeftWalkPoint.transform));
	}
	
	IEnumerator StartLevel()
	{
		yield return new WaitForSeconds(0.5f);
		fadeQuad.GetComponent<Renderer>().material.color = Color.Lerp(fadeQuad.GetComponent<Renderer>().material.color, Color.clear, 2 * Time.deltaTime);
		yield return new WaitForSeconds(0.5f);
		fadeQuad.GetComponent<Renderer> ().material.color = Color.clear;
		levelStart = true;
	}
	
	IEnumerator FinishLevel()
	{
		if (once) {
			AudioManager.instance.PlaySFXClip (AudioManager.instance.spark1, AudioManager.instance.sfxMixer);
			once = false;
		}
		
		
		string levelName = Application.loadedLevelName;
		string[] levelNameWords = levelName.Split (new char[]{'_'});
		int levelNumber = 1;
		int.TryParse (levelNameWords [levelNameWords.Length - 1], out levelNumber);
		int nextLevelNumber = levelNumber + 1;

		if (nextLevelNumber > PlayerPrefs.GetInt ("highestLevel"))
			PlayerPrefs.SetInt ("highestLevel", nextLevelNumber);
		
		if (this.gameObject.GetComponent<NeighbourLeftSystem>().debugOn)
			print ("("+Application.loadedLevelName+")Finished Level " + levelNumber + ", Loading Level " + nextLevelNumber);
		
		yield return new WaitForSeconds(1);
		fadeQuad.GetComponent<Renderer>().material.color = Color.Lerp(fadeQuad.GetComponent<Renderer>().material.color, Color.black, 2 * Time.deltaTime);
		yield return new WaitForSeconds(2);
		
		
		if (nextLevelNumber <29)
			Application.LoadLevel ("Level_" + nextLevelNumber.ToString ());
		else
		{
			/// Level to load after finished final level.
			Application.LoadLevel ("Start_Scene");
		}

	}
	
	IEnumerator MoveFromTo(Transform from, Transform to)
	{	
		notMoving = false;
		flag = false;
		
		float frac = 0f;
		
		GameObject rampPointLow = null;
		GameObject rampPointHigh = null;
		
		if (from.transform.parent.parent.name == "Ramp" && to.transform.parent.parent.name != "Ramp")
		{	
			rampPointLow = null;
			rampPointHigh = null;
			
			foreach (Transform point in from.transform.parent)
			{
				if (point.name == "RampWalkPoint" && point.tag == "Low")
					rampPointLow = point.gameObject;
				else if (point.name == "RampWalkPoint" && point.tag == "High")
					rampPointHigh = point.gameObject;
			}
			
			float distLow = Vector3.Distance(rampPointLow.transform.position, to.transform.position);
			float distHigh = Vector3.Distance(rampPointHigh.transform.position, to.transform.position);
			
			bool travellingDown;
			
			if (distLow < distHigh)
				travellingDown = true;
			else
				travellingDown = false;
			
			if (travellingDown)
			{
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed / (Mathf.Sqrt (2f)/2);
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(from.position, rampPointLow.transform.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
				frac = 0f;
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed * 2;
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(rampPointLow.transform.position, to.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
			}
			else
			{
				
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed / (Mathf.Sqrt (2f)/2);
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(from.position, rampPointHigh.transform.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
				frac = 0f;
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed * 2;
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(rampPointHigh.transform.position, to.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
			}
		}
		else if (from.transform.parent.parent.name != "Ramp" && to.transform.parent.parent.name == "Ramp")
		{
			rampPointLow = null;
			rampPointHigh = null;
			
			foreach (Transform point in to.transform.parent)
			{
				if (point.name == "RampWalkPoint" && point.tag == "Low")
					rampPointLow = point.gameObject;
				else if (point.name == "RampWalkPoint" && point.tag == "High")
					rampPointHigh = point.gameObject;
			}
			
			float distLow = Vector3.Distance(rampPointLow.transform.position, from.transform.position);
			float distHigh = Vector3.Distance(rampPointHigh.transform.position, from.transform.position);
			
			bool travellingDown;
			
			if (distLow < distHigh)
				travellingDown = false;
			else
				travellingDown = true;
			
			if (travellingDown)
			{
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed * 2;
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(from.position, rampPointHigh.transform.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
				frac = 0f;
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed / (Mathf.Sqrt (2f)/2);
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(rampPointHigh.transform.position, to.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
			}
			else
			{
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed * 2;
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(from.position, rampPointLow.transform.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
				frac = 0f;
				while(frac < 1)
				{
					float newSpeed = speed * Time.deltaTime;
					newSpeed = newSpeed / 10;
					
					newSpeed = newSpeed / (Mathf.Sqrt (2f)/2);
					
					frac = frac + newSpeed;
					
					this.transform.position = Vector3.Lerp(rampPointLow.transform.position, to.position, frac);
					
					if (!easyEase)
					{
						if (frac >= 1)
						{
							if (this.transform.position != to.position)
								this.transform.position = to.position;
							notMoving = true;
						}
						else
						{
							yield return null;
						}
					}
				}
			}
		}
		else if (from.transform.parent.parent.name == "Ramp" && to.transform.parent.parent.name == "Ramp")
		{
			while(frac < 1)
			{
				float newSpeed = speed * Time.deltaTime;
				newSpeed = newSpeed / 10;
				
				newSpeed = newSpeed / Mathf.Sqrt (2f);
				
				frac = frac + newSpeed;
				
				this.transform.position = Vector3.Lerp(from.position, to.position, frac);
				
				if (!easyEase)
				{
					if (frac >= 1)
					{
						if (this.transform.position != to.position)
							this.transform.position = to.position;
						notMoving = true;
					}
					else
					{
						yield return null;
					}
				}
			}
		}
		else if (from.transform.parent.parent.name != "Ramp" && to.transform.parent.parent.name != "Ramp")
		{
			while(frac < 1)
			{
				float newSpeed = speed * Time.deltaTime;
				newSpeed = newSpeed / 10;
				
				frac = frac + newSpeed;
				
				this.transform.position = Vector3.Lerp(from.position, to.position, frac);
				
				if (!easyEase)
				{
					if (frac >= 1)
					{
						if (this.transform.position != to.position)
							this.transform.position = to.position;
						notMoving = true;
					}
					else
					{
						yield return null;
					}
				}
			}
		}
		else
		{
			print ("Error.");
		}
		notMoving = true;
		teleportStarted = false;
		
		neighbourLeftSystem.ReCall ();
	}
	
	public IEnumerator RotateTo(int idNext)
	{
		if (idNext >= 0)
		{
			float frac = 0f;
			
			float degTo = 0;
			
			degTo = (idNext * 90) + 90;
			degTo = Mathf.Round (degTo * 10) / 10;
			
			Quaternion rotTo = Quaternion.Euler (0f, degTo, 0f);
			
			while(frac < 1)
			{
				float newSpeed = speed * Time.deltaTime;
				newSpeed = newSpeed / 10;
				
				frac = frac + newSpeed;
				
				this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotTo, frac);
				
				yield return null;
			}
		}
	}
	
	public IEnumerator RotateToLook(GameObject lookPoint)
	{
		rotatingToLook = true;
		float frac = 0f;
		
		Vector3 targetPosition = new Vector3(lookPoint.transform.position.x, transform.position.y, lookPoint.transform.position.z);
		
		Quaternion targetRotation = Quaternion.LookRotation (targetPosition - transform.position);
		
		while(frac < 1)
		{
			float newSpeed = (speed * Time.deltaTime) / 2;
			newSpeed = newSpeed / 10;
			
			frac = frac + newSpeed;
			
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, frac);
			
			rotatingToLook = false;
			
			yield return null;
		}
	}
	
	void RenderAbove()
	{
		if (this.gameObject.transform.position.y == 10.5f ||
		    this.gameObject.transform.position.y == 9.5f ||
		    this.gameObject.transform.position.y == 8.5f ||
		    this.gameObject.transform.position.y == 7.5f ||
		    this.gameObject.transform.position.y == 6.5f ||
		    this.gameObject.transform.position.y == 5.5f ||
		    this.gameObject.transform.position.y == 4.5f ||
		    this.gameObject.transform.position.y == 3.5f ||
		    this.gameObject.transform.position.y == 2.5f ||
		    this.gameObject.transform.position.y == 1.5f ||
		    this.gameObject.transform.position.y == 0.5f ||
		    this.gameObject.transform.position.y == -10.5f ||
		    this.gameObject.transform.position.y == -9.5f ||
		    this.gameObject.transform.position.y == -8.5f ||
		    this.gameObject.transform.position.y == -7.5f ||
		    this.gameObject.transform.position.y == -6.5f ||
		    this.gameObject.transform.position.y == -5.5f ||
		    this.gameObject.transform.position.y == -4.5f ||
		    this.gameObject.transform.position.y == -3.5f ||
		    this.gameObject.transform.position.y == -2.5f ||
		    this.gameObject.transform.position.y == -1.5f ||
		    this.gameObject.transform.position.y == -0.5f
		    )
		{
			ReRender (this.gameObject, 0);
		}
		else
		{
			ReRender (this.gameObject, 14);
		}
	}
	
	void ReRender(GameObject go, int layer)
	{
		go.gameObject.layer = layer;
		foreach (Transform trans in go.transform)
		{
			ReRender (trans.gameObject, layer);
		}
	}
	
	bool RenderCheck()
	{
		RaycastHit baseHit;
		Vector3 newVec = new Vector3 (this.transform.position.x, this.transform.position.y + 0.4f, this.transform.position.z);
		Ray rayBase = new Ray(newVec, (this.transform.TransformDirection (rotateCam.CamQua * Vector3.back)));
		Physics.Raycast (rayBase, out baseHit, Mathf.Infinity, 1 << 15);
		Debug.DrawRay (rayBase.origin, rayBase.direction, Color.red, 1f);
		if ((baseHit.collider != null) && baseHit.collider.transform.parent.parent.name != "Ramp")
			return false;
		else
			return true;
		
	}
}
