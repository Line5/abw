﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SaveLoadLock : MonoBehaviour {

	public GameObject[] levelLines;
	public GameObject[] worldLines;
	public GameObject[] selectWorldFrames;
	public GameObject[] selectLevelFrames;
	public int highestLevelInput;
	private int highestLevel;
	private int lastInt = 0;

	void Start ()
	{
		highestLevel = highestLevelInput;
		LoadLevelSelection ();
	}

	void Update ()
	{
		highestLevel = highestLevelInput;

		if (lastInt != highestLevel)
		{
			LoadLevelSelection ();
			lastInt = highestLevel;
		}
	}

	void LoadLevelSelection ()
	{
		foreach (GameObject frame in selectWorldFrames)
		{
			frame.GetComponent<Button>().interactable = false;
			
			foreach (Transform lockSprite in frame.transform)
			{
				if (lockSprite.gameObject != null)
					lockSprite.gameObject.SetActive(true);
			}
		}

		foreach (GameObject frame in selectLevelFrames)
		{
			frame.GetComponent<Button>().interactable = false;

			foreach (Transform lockSprite in frame.transform)
			{
				if (lockSprite.gameObject != null)
					lockSprite.gameObject.SetActive(true);
			}
		}

		if (highestLevelInput == 0)
		{
			if (PlayerPrefs.HasKey ("highestLevel"))
				highestLevel = PlayerPrefs.GetInt ("highestLevel");
		}

		switch (highestLevel)
		{
		case 1:
			UnlockWorlds (0);
			UnlockLevels (1);
			break;
		case 2:
			UnlockWorlds (0);
			UnlockLevels (2);
			break;
		case 3:
			UnlockWorlds (0);
			UnlockLevels (3);
			break;
		case 4:
			UnlockWorlds (1);
			UnlockLevels (4);
			break;
		case 5:
			UnlockWorlds (1);
			UnlockLevels (5);
			break;
		case 6:
			UnlockWorlds (1);
			UnlockLevels (6);
			break;
		case 7:
			UnlockWorlds (1);
			UnlockLevels (7);
			break;
		case 8:
			UnlockWorlds (1);
			UnlockLevels (8);
			break;
		case 9:
			UnlockWorlds (2);
			UnlockLevels (9);
			break;
		case 10:
			UnlockWorlds (2);
			UnlockLevels (10);
			break;
		case 11:
			UnlockWorlds (2);
			UnlockLevels (11);
			break;
		case 12:
			UnlockWorlds (2);
			UnlockLevels (12);
			break;
		case 13:
			UnlockWorlds (2);
			UnlockLevels (13);
			break;
		case 14:
			UnlockWorlds (3);
			UnlockLevels (14);
			break;
		case 15:
			UnlockWorlds (3);
			UnlockLevels (15);
			break;
		case 16:
			UnlockWorlds (3);
			UnlockLevels (16);
			break;
		case 17:
			UnlockWorlds (3);
			UnlockLevels (17);
			break;
		case 18:
			UnlockWorlds (3);
			UnlockLevels (18);
			break;
		case 19:
			UnlockWorlds (4);
			UnlockLevels (19);
			break;
		case 20:
			UnlockWorlds (4);
			UnlockLevels (20);
			break;
		case 21:
			UnlockWorlds (4);
			UnlockLevels (21);
			break;
		case 22:
			UnlockWorlds (4);
			UnlockLevels (22);
			break;
		case 23:
			UnlockWorlds (4);
			UnlockLevels (23);
			break;
		case 24:
			UnlockWorlds (5);
			UnlockLevels (24);
			break;
		case 25:
			UnlockWorlds (5);
			UnlockLevels (25);
			break;
		case 26:
			UnlockWorlds (5);
			UnlockLevels (26);
			break;
		case 27:
			UnlockWorlds (5);
			UnlockLevels (27);
			break;
		case 28:
			UnlockWorlds (5);
			UnlockLevels (28);
			break;

		default :
			if (highestLevel == 0)
			{
				UnlockWorlds (0);
				UnlockLevels (1);
			}
			else if (highestLevel > 28)
			{
				UnlockWorlds (5);
				UnlockLevels (28);
			}
			break;
		}

	}

	void UnlockWorlds(int j)
	{
		if (j > 5)
			j = 5;

		for (int i = 0; i <= 5; i++)
		{
			if (i <= j)
			{
				selectWorldFrames[i].GetComponent<Button>().interactable = true;

				selectWorldFrames[i].transform.GetChild(0).gameObject.SetActive(false);
				if (i-1 >= 0)
					worldLines[i-1].SetActive(true);
			}
			else
			{
				selectWorldFrames[i].transform.GetChild(1).gameObject.SetActive(false);
				//worldLines[i].SetActive(false);
			}

			/*foreach (Transform lockSprite in selectWorldFrames[i].transform)
			{
				if (lockSprite.gameObject != null)
					lockSprite.gameObject.SetActive(false);
			}*/
		}
	}

	void UnlockLevels(int j)
	{
		if (j > 28)
			j = 28;
		j--;

		for (int i = 0; i <= 27; i++)
		{


			if (i <= j)
			{
				selectLevelFrames[i].GetComponent<Button>().interactable = true;
				
				selectLevelFrames[i].transform.GetChild(0).gameObject.SetActive(false);
				if (i-1 >= 0)
					levelLines[i-1].SetActive(true);
				//levelLines[i].SetActive(true);
			}
			else
			{
				selectLevelFrames[i].transform.GetChild(1).gameObject.SetActive(false);
			}
			/*foreach (Transform lockSprite in selectLevelFrames[i].transform)
			{
				if (lockSprite.gameObject != null)
					lockSprite.gameObject.SetActive(false);
			}*/
		}
	}
}
