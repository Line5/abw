﻿using UnityEngine;
using System.Collections;

public class UIPlayAnimation : MonoBehaviour {

	public void OnDownAnimation()
	{
		this.gameObject.GetComponent<Animator> ().SetTrigger ("ScaleIn");
	}
	public void OnUpAnimation()
	{
		this.gameObject.GetComponent<Animator> ().SetTrigger ("Spin");
	}
}
