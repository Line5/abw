﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TransformableIsland : MonoBehaviour
{
	public FireAndFan[] fanContainer;

	public List<GameObject> islandFBXs = new List<GameObject> ();
	public Color islandNormalColor = new Color(0f / 225f,59f / 225f,71f / 225f,0f / 225f);
	public Color islandBlockedColor = new Color(62f / 225f,0f / 225f,0f / 225f,0f / 225f);
	public Color islandHighlightedColor = new Color(0f / 225f,95f / 225f,114f / 225f,0f / 225f);
	private bool onceColor = true;

	private bool check = false;
	private bool click = false;

	private Vector2 posMouseDown;
	private Vector2 posMouse;
	private float posDif = 0f;
	private float posDifUp = 0f;

	public bool directionTrueIsX;
	public int clampPos;
	public int clampNeg;

	private NeighbourLeftSystem neighbourLeftSystem;

	void Start()
	{
		neighbourLeftSystem = GameObject.Find ("Player").GetComponent<NeighbourLeftSystem> ();
/*		foreach (Transform transform in this.transform)
		{
			if (transform.name == "Props")
			{
				foreach (Transform block in transform)
				{
					islandFBXs.Add (block.gameObject);
				}
			}
		}
*/	}

	void Update ()
	{
		Move ();
	}

	public bool Check
	{
		get
		{
			return check;
		}
		set
		{
			check = value;
		}
	}

	void Move()
	{
		if (islandFBXs.Count() > 0)
		{
			foreach (GameObject islandFBX in islandFBXs)
			{
				if (onceColor)
				{
					Renderer rend = islandFBX.GetComponent<Renderer>();
					rend.material.EnableKeyword ("_EMISSION"); 
					rend.material.SetColor ("_EmissionColor", islandNormalColor);
					onceColor = false;
				}

				if (Input.GetMouseButton(0) && !PlayerCheck ())
				{

					Renderer rend = islandFBX.GetComponent<Renderer>();
					rend.material.EnableKeyword ("_EMISSION"); 
					rend.material.SetColor ("_EmissionColor", islandBlockedColor);
				}

				if (Input.GetMouseButton(0) && PlayerCheck ())
				{

					Renderer rend = islandFBX.GetComponent<Renderer>();
					rend.material.EnableKeyword ("_EMISSION"); 
					rend.material.SetColor ("_EmissionColor", islandHighlightedColor);
				}

				if (Input.GetMouseButtonUp(0))
				{
					Renderer rend = islandFBX.GetComponent<Renderer>();
					rend.material.EnableKeyword ("_EMISSION"); 
					rend.material.SetColor ("_EmissionColor", islandNormalColor);
				}
			}
		}

		if (Input.GetMouseButtonDown(0) && PlayerCheck())
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			Physics.Raycast (ray.origin, ray.direction, out hit, Mathf.Infinity, 1 << 11);

			Collider[] cols = this.gameObject.GetComponents<Collider>();

			foreach(Collider col in cols)
			{
				if(hit.collider == col)
				{
					click = true;
					if (this.gameObject.name == "Rotate")
						posDifUp = posDif;
					else
					{
						if (directionTrueIsX)
							posDifUp = this.transform.position.x;
						else
							posDifUp = this.transform.position.z;
					}
					posMouseDown = Input.mousePosition;
				}
			}
		}

		if (this.gameObject.name == "Rotate")
		{
			if ((Input.GetMouseButton (0) && PlayerCheck()) && click)
			{
				posMouse = Input.mousePosition;

				if (posMouseDown.y >= Camera.main.WorldToScreenPoint(this.gameObject.transform.position).y)
					posDif = (posMouse.x - posMouseDown.x) + posDifUp;
				else
					posDif = (- posMouse.x + posMouseDown.x) + posDifUp;

				foreach (Transform path in this.transform)
				{
					foreach (Transform child in path)
						if (child.name == "PointSystem")
							child.gameObject.SetActive (false);
				}

				Quaternion rotation = Quaternion.Euler(0f, posDif, 0f);
				this.gameObject.transform.rotation = rotation;
			}
			if ((Input.GetMouseButtonUp (0) & PlayerCheck()) && click)
			{
				Quaternion finalRot = this.gameObject.transform.rotation;
				float finalRotY = finalRot.eulerAngles.y;
				float temp = 0f;

				if (0f <= finalRotY && finalRotY < 45f)
					temp = 0f;
				if (45f <= finalRotY && finalRotY < 135f)
					temp = 90.0f;
				if (135f <= finalRotY && finalRotY < 225f)
					temp = 180.0f;
				if (225f <= finalRotY && finalRotY < 315f)
					temp = 270.0f;
				if (315f <= finalRotY && finalRotY < 360f)
					temp = 0f;

				finalRot = Quaternion.Euler (0f, temp, 0f);

				posDif = temp;

				this.gameObject.transform.rotation = finalRot;

				foreach (Transform path in this.transform)
				{
					foreach (Transform child in path)
						if (child.name == "PointSystem")
							child.gameObject.SetActive (true);
				}

				ReAlignCastPoints();

				click = false;
//				rotateCam.RecalculateNeighbours ();

				if (fanContainer.Length > 0)
				{
					foreach (FireAndFan script in fanContainer)
						script.StartCoroutine ("DoFan");
				}
				
				neighbourLeftSystem.ReCall ();
			}
		}

		if (this.gameObject.name == "Slide")
		{

			if ((Input.GetMouseButton (0) && PlayerCheck()) && click)
			{
				posMouse = Input.mousePosition;
				posDif = (posDifUp-(posMouseDown.x - posMouse.x)/50);

				if (directionTrueIsX)
				{
					if (Camera.main.transform.rotation.eulerAngles.y > 130f && Camera.main.transform.rotation.eulerAngles.y < 230f)
					{	
						posDif = (posDifUp-(posMouse.x - posMouseDown.x)/50);
					}
				}
				else if (!directionTrueIsX)
				{
					if (Camera.main.transform.rotation.eulerAngles.y > 220f && Camera.main.transform.rotation.eulerAngles.y < 320f)
					{	
						posDif = (posDifUp-(posMouseDown.x - posMouse.x )/50);
					}
					else
					{
						posDif = (posDifUp-(posMouse.x - posMouseDown.x)/50);
					}
				}

				posDif = Mathf.Clamp (posDif, clampNeg, clampPos);

				foreach (Transform path in this.transform)
				{
					foreach (Transform child in path)
						if (child.name == "PointSystem")
							child.gameObject.SetActive (false);
				}

				Vector3 position;

				if (directionTrueIsX)
					position = new Vector3(posDif, this.transform.position.y, this.transform.position.z);
				else
					position = new Vector3(this.transform.position.x, this.transform.position.y, posDif);

				this.gameObject.transform.position = position;
			}

			if ((Input.GetMouseButtonUp (0) && PlayerCheck()) && click)
			{
				Vector3 finalPosition = new Vector3(Mathf.RoundToInt(this.transform.localPosition.x), Mathf.RoundToInt(this.transform.localPosition.y), Mathf.RoundToInt(this.transform.localPosition.z));
				this.gameObject.transform.localPosition = finalPosition;

				foreach (Transform path in this.transform)
				{
					foreach (Transform child in path)
						if (child.name == "PointSystem")
							child.gameObject.SetActive (true);
				}

				click = false;

				if (fanContainer.Length > 0)
				{
					foreach (FireAndFan script in fanContainer)
						script.StartCoroutine ("DoFan");
				}
				
				neighbourLeftSystem.ReCall ();
			}
		}
	}

	bool PlayerCheck()
	{
		bool temp = true;

		foreach(Transform path in this.transform)
		{
			foreach (Transform child in path)
			{
				if (child.name == "PointSystem")
				{
					if (child.gameObject.tag == "NextLeft" || child.gameObject.tag == "Occupied") // child.gameObject.tag == "NextLeft" ||
					{
						temp = false;
					}
				}
			}
		}

		return temp;
	}

	void ReAlignCastPoints()
	{
		foreach (Transform path in this.gameObject.transform)
		{
			if (path.name != "Ramp")
			{
				GameObject pointSystem = null;

				foreach (Transform child in path)
					if (child.name == "PointSystem")
						pointSystem = child.gameObject;

				Quaternion antiRot = Quaternion.Euler (0f, 0f, 0f);
				if (pointSystem != null)
					pointSystem.transform.rotation = antiRot;
			}
			else
			{
				List<GameObject> list = new List<GameObject>();

				foreach (Transform child in path)
					if (child.name == "PointSystem")
						foreach (Transform transform in child)
							if (transform.name == "Cast Point")
								list.Add (transform.gameObject);

				List<GameObject> newList = list.OrderBy(GameObject => GameObject.transform.position.x).ThenBy (GameObject => GameObject.transform.position.z).ToList();
				newList.Reverse ();
				GameObject temp;
				temp = newList [1];
				newList[1] = newList[3];
				newList [3] = temp;
				temp = newList [1];
				newList[1] = newList[2];
				newList [2] = temp;
				int i = 0;
				foreach (GameObject castPoint in newList)
				{
					// Tags CastPoints According to their position
					if(i == 0)
						castPoint.tag = "Pos X";
					if(i == 1)
						castPoint.tag = "Neg Z";
					if(i == 2)
						castPoint.tag = "Neg X";
					if(i == 3)
						castPoint.tag = "Pos Z";
					i++;
				}
			}
		}
	}
}
