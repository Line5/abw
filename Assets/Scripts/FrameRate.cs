﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FrameRate : MonoBehaviour {

	private Text frameDisplay;
	private float fps = 0.0f;
	private NeighbourLeftSystem playerDebug;

	void Start ()
	{
		frameDisplay = this.GetComponent<Text> ();
		playerDebug = GameObject.Find ("Player").GetComponent<NeighbourLeftSystem> (); 
	}
	
	void Update ()
	{
		if (playerDebug.debugOn)
		{
			fps += (Time.deltaTime - fps);
			fps = fps * 1000;
			fps = Mathf.Round(fps * 100f) / 100f;

			frameDisplay.text = fps + "fps";
		}
		else
		{
			frameDisplay.text = "";
		}
	}
}
