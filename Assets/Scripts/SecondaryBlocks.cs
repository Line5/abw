﻿using UnityEngine;
using System.Collections;

public class SecondaryBlocks : MonoBehaviour {

	private GameObject player;
	private PlayerMovement playerMovement;
	private RotateCam rotateCam;

	public GameObject[] deadPixelVisualGameobjects;
	public Material deadMaterial;
	public Material aliveMaterial;
	private bool dead90Or180 = false;
	private bool deadOn = false;

	public bool fireClear = false;
	public Material fireMaterial;
	public GameObject fireModel;
	
	public Material teleportMaterial;

	public Material buttonMaterial;
	public GameObject buttonPlatform;

	public Material speedMaterial;

	public bool TriggerTrue
	{
		set
		{
			if (this.transform.parent.name == "Fire")
				fireClear = true;
		}
	}

	void Start()
	{
		player = GameObject.Find("Player");
		playerMovement = player.GetComponent<PlayerMovement> ();

		if (this.transform.parent.name == "Dead90")
			dead90Or180 = false;
		else if (this.transform.parent.name == "Dead180")
			dead90Or180 = true;
	}

	void Update ()
	{
		if (dead90Or180)
		{
			if ((Camera.main.transform.rotation.eulerAngles.y < 90 && Camera.main.transform.rotation.eulerAngles.y > 0 ) || (Camera.main.transform.rotation.eulerAngles.y < 270 && Camera.main.transform.rotation.eulerAngles.y > 180 ))
			{
				deadOn = true;
			}
			else
			{
				deadOn = false;
			}
		}
		else
		{
			if ((Camera.main.transform.rotation.eulerAngles.y < 90 && Camera.main.transform.rotation.eulerAngles.y > 0 ) || (Camera.main.transform.rotation.eulerAngles.y < 270 && Camera.main.transform.rotation.eulerAngles.y > 180 ))
			{
				deadOn = false;
			}
			else
			{
				deadOn = true;
			}
		}

		if (this.transform.parent.name == "Dead90" || this.transform.parent.name == "Dead180")
		{
			if (!playerMovement.DeadActive)
			{
				if (deadOn)
				{
					this.transform.parent.GetComponent<Renderer>().material = aliveMaterial;

					foreach (Transform point in this.transform)
					{
						point.gameObject.SetActive (true);
					}
				}
				else
				{
					this.transform.parent.GetComponent<Renderer>().material = deadMaterial;
					foreach (Transform point in this.transform)
					{
						point.gameObject.SetActive (false);
					}
				}
			}
		}

		if (this.transform.parent.name == "Speed")
		{
			this.transform.parent.GetComponent<Renderer>().material = speedMaterial;
		}

		if (this.transform.parent.name == "Fire")
		{
			if (fireClear)
			{
				this.transform.parent.GetComponent<Renderer>().material = aliveMaterial;

				if(fireModel != null)
					fireModel.gameObject.SetActive (false);

				foreach (Transform point in this.transform)
				{
					point.gameObject.SetActive (true);
				}
			}
			else
			{
				this.transform.parent.GetComponent<Renderer>().material = fireMaterial;
				foreach (Transform point in this.transform)
				{
					if (point.name == "Occupied Point")
						point.gameObject.SetActive (false);
				}
			}
		}

		if (this.transform.parent.name == "Teleport")
		{
			this.transform.parent.GetComponent<Renderer>().material = teleportMaterial;

			GameObject teleportToPointSystem = this.transform.parent.GetComponent<TeleportScript>().RecieveTeleportPointSystem;
			if (teleportToPointSystem != null)
			{

			}
		}
	}
}
