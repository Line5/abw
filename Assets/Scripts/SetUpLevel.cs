﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SetUpLevel : MonoBehaviour
{
	public GameObject pointSystem;
	public GameObject pointSystemRamp;
	public GameObject pointSystemExit;

	public GameObject spriteManager;

	private Transform level;
	private List<GameObject> tempCastPointList = new List<GameObject>();

	public Material materialSlide;
	public Material materialRotate;
	public Material materialExit;

	void Awake () {
	
		level = GameObject.Find ("Level").transform;
		SetupPaths ();
	}

	void SetupPaths()
	{
		foreach (Transform transform in level)
		{
			if(transform.gameObject.name == "Static")
			{
				foreach (Transform path in transform)
				{
					AddComponents (path);

					if (path.name != "Exit" && path.name != "Props")
						ShuffleCastPoints(tempCastPointList);
				}
			}
			else if(transform.gameObject.name == "Slide")
			{
				transform.gameObject.layer = 11;

				foreach (Transform path in transform)
				{
					AddComponents (path);

					if (path.name != "Exit" && path.name != "Props")
						ShuffleCastPoints(tempCastPointList);

					SphereCollider sc = path.parent.gameObject.AddComponent<SphereCollider>();
					sc.radius = 1.5f / path.parent.gameObject.transform.localScale.x;
					sc.center = path.localPosition;

//					Renderer rend = path.GetComponent<Renderer>();
//					rend.material = materialSlide;
				}
			}
			else if(transform.gameObject.name == "Rotate")
			{
				transform.gameObject.layer = 11;

				foreach (Transform path in transform)
				{
					AddComponents (path);
					
					if (path.name != "Exit" && path.name != "Props")
						ShuffleCastPoints(tempCastPointList);
					
					SphereCollider sc = path.parent.gameObject.AddComponent<SphereCollider>();
					sc.radius = 1.5f;
					sc.center = path.localPosition;

//					Renderer rend = path.GetComponent<Renderer>();
//					rend.material = materialRotate;
				}
			}
		}
	}

	// Shuffles CastPoints in the order of their Position, (+X, -Z, -X, +Z), and tags them to match.
	void ShuffleCastPoints(List<GameObject> list)
	{
		List<GameObject> newList = list.OrderBy(GameObject => GameObject.transform.position.x).ThenBy (GameObject => GameObject.transform.position.z).ToList();
		newList.Reverse ();
		GameObject temp;
		temp = newList [1];
		newList[1] = newList[3];
		newList [3] = temp;
		temp = newList [1];
		newList[1] = newList[2];
		newList [2] = temp;
		int i = 0;
		foreach (GameObject castPoint in newList)
		{
			// Tags CastPoints According to their position
			if(i == 0)
				castPoint.tag = "Pos X";
			//				castPoint.tag = "test";
			if(i == 1)
				castPoint.tag = "Neg Z";
			//				castPoint.tag = "test";
			if(i == 2)
				castPoint.tag = "Neg X";
			//				castPoint.tag = "test";
			if(i == 3)
				castPoint.tag = "Pos Z";
			//				castPoint.tag = "test";
			i++;
		}
		tempCastPointList.Clear ();
	}

	void AddComponents (Transform path)
	{
		if (path.name == "Ramp")
		{
			
			GameObject temp = Instantiate (pointSystemRamp, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Path")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Exit")
		{
			GameObject temp = Instantiate (pointSystemExit, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					float degree = path.rotation.eulerAngles.y;

					if(degree > 315 || degree <= 45)
						trans.tag = "Pos X";
					if(degree > 45 && degree <= 135)
						trans.tag = "Neg Z";
					if(degree > 135 && degree <= 225)
						trans.tag = "Neg X";
					if(degree > 225 && degree <= 315)
						trans.tag = "Pos Z";
				}
			}

			Renderer rend = path.GetComponent<Renderer>();
			rend.material = materialExit;
		}
		else if (path.name == "Button")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Speed")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Fire")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";

//			SphereCollider sc = temp.AddComponent<SphereCollider>() as SphereCollider;

			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Teleport")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
		else if (path.name == "Dead90" || path.name == "Dead180")
		{
			GameObject temp = Instantiate (pointSystem, path.position, path.rotation) as GameObject;
			temp.transform.parent = path;
			temp.name = "PointSystem";
			foreach (Transform trans in temp.transform)
			{
				if (trans.name == "Cast Point")
				{
					tempCastPointList.Add (trans.gameObject);
				}
			}
		}
	}
}
