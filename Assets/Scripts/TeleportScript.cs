using UnityEngine;
using System.Collections;

public class TeleportScript : MonoBehaviour {

	public GameObject TeleportTo;

	public GameObject RecieveTeleportPointSystem
	{
		get
		{
			if (TeleportTo != null)
			{
				foreach (Transform child in TeleportTo.transform)
				{
					if (child.name == "PointSystem" && child.gameObject.activeSelf)
						return child.gameObject;
					else
						return null;
				}
				return null;
			}
			else
			{
				print ("ERROR: Teleporter id:(" + this.gameObject.GetInstanceID() + ") does not have a sister Point System.");
				foreach (Transform child in TeleportTo.transform)
				{
					if (child.name == "PointSystem" && child.gameObject.activeSelf)
						return child.gameObject;
					else
						return null;
				}
				return null;
			}
		}
	}
}
